<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://saemm.gob.mx" prefix="patterns"%>

<%@include file="/encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-sis-min.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="/migas.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>Plataforma Digital Estatal</h1>
            <h2>Sistema II</h2>
            <h4>Servidores p&uacute;blicos que intervengan en procedimientos de contrataciones p&uacute;blicas.</h4>
            <div id="cargaDB" class="reveal-overlay" style="display: none;">
                <div id="loader"></div>
                <div id="textDB">
                    Actualizando base de datos...
                </div>
            </div>
            <div class="stacked-for-small expanded large button-group">
                <a class="button hollow" onclick="document.frmListado.submit()"><i class="material-icons md-4t">list</i> Listado de servidores p&uacute;blicos</a>
                <c:if test="${nivelDeAcceso.id == 1 || nivelDeAcceso.id == 2}">
                    <a class="button hollow" onclick="document.frmNuevo.submit()"><i class="material-icons md-4t">playlist_add</i> Agregar nuevo servidor p&uacute;blico</a>
                    <form name="frmNuevo" method="post" action="/sistemaCaptura${initParam.versionIntranet}/contrataciones">
                        <input type="hidden" name="accion" value="mostrarPaginaRegistroNuevaContrataciones"/>
                    </form>
                </c:if>
            </div>
            <h2>Datos del servidor p&uacute;blico </h2>
            <fieldset class="fieldset">
                <legend>Servidor p&uacute;blico</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>Ejercicio fiscal</b>
                        <label>${contrataciones.ejercicioFiscal}</label>
                    </label>
                    <label class="cell medium-4"><b>Nombres</b>
                        <label>${contrataciones.nombres}</label>
                    </label>
                    <label class="cell medium-4"><b>Primer apellido</b>
                        <label>${contrataciones.primerApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>Segundo apellido</b>
                        <label>${contrataciones.segundoApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>RFC</b>
                        <label>${patterns:validarRfcPF(contrataciones.rfc) ? contrataciones.rfc : ""}</label>
                    </label>
                    <label class="cell medium-4"><b>CURP</b>
                        <label>${patterns:validarCurp(contrataciones.curp) ? contrataciones.curp : ""}</label>
                    </label>
                    <label class="cell medium-4"><b>G&eacute;nero</b>
                        <label>${contrataciones.genero.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Ramo</b>
                        <label>${contrataciones.ramo.clave} - ${contrataciones.ramo.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Ente p&uacute;blico</b>
                        <label>${contrataciones.institucionDependencia.nombre}</label>
                    </label>
                    <label class="cell medium-4"><b>Puesto</b>
                        <label>${contrataciones.puesto.nombre}</label>
                    </label>
                    <label class="cell medium-4"><b>Tipo de &aacute;rea</b>
                        <ul>
                            <c:forEach items="${contrataciones.tipoArea}" var="area" varStatus="contador">
                                <li><label>${area.valor}${!contador.last ? ',' : ''}</label></li>
                                    </c:forEach>
                        </ul>
                    </label>
                    <label class="cell medium-4"><b>Tipo de procedimiento</b>
                        <ul>
                            <c:forEach items="${contrataciones.tipoProcedimiento}" var="tipoProcedimiento">
                                <li><label>${tipoProcedimiento.valor}</label></li>
                                    </c:forEach>
                        </ul>
                    </label>
                    <label class="cell medium-4"><b>Nivel de responsabilidad</b>
                        <ul>
                            <c:forEach items="${contrataciones.nivelResponsabilidad}" var="nivelResponsabilidad" varStatus="contador" >
                                <li><label>${nivelResponsabilidad.valor}${!contador.last ? ',' : ''}</label></li>
                                    </c:forEach>
                        </ul>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Superior inmediato</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>Nombres</b>
                        <label>${contrataciones.superiorInmediato.nombres}</label>
                    </label>
                    <label class="cell medium-4"><b>Primer apellido</b>
                        <label>${contrataciones.superiorInmediato.primerApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>Segundo apellido</b>
                        <label>${contrataciones.superiorInmediato.segundoApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>CURP</b>
                        <label>${contrataciones.superiorInmediato.curp}</label>
                    </label>
                    <label class="cell medium-4"><b>RFC</b>
                        <label>${contrataciones.superiorInmediato.rfc}</label>
                    </label>
                    <label class="cell medium-4"><b>Puesto</b>
                        <label>${contrataciones.superiorInmediato.puesto.nombre}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Observaciones</legend>
                <label class="cell">
                    <label>${contrataciones.observaciones}</label>
                </label>
            </fieldset>
            <a class="button expanded" onclick="frmRegresar.submit()">Regresar</a>
        </div>
    </div>
</div>
<form name="frmListado" method="post" action="/sistemaCaptura${initParam.versionIntranet}/contrataciones">
    <input type="hidden" name="accion" value="ingresarSistema"/>
</form>
<form name="frmRegresar" action="/sistemaCaptura${initParam.versionIntranet}/contrataciones" method="post">
    <input type="hidden" name="accion" value="consultarServidoresPublicosEnContrataciones"/>
    <input type="hidden" name="txtID" value="${servidorPublico.id}"/>
    <input type="hidden" name="txtNombre" value="${filtro.NOMBRES}"/>
    <input type="hidden" name="txtPrimerApellido" value="${filtro.PRIMER_APELLIDO}"/>
    <input type="hidden" name="txtOficina" value="${filtro.INSTITUCION_DEPENDENCIA}"/>
    <input type="hidden" name="cmbPaginacion" value="${filtro.registrosMostrar}"/>
    <input type="hidden" name="cmbOrdenacion" value="${filtro.orden}"/>
    <input type="hidden" name="txtNumeroPagina" value="${filtro.numeroPagina}"/>
</form>

<%@include file="/piePagina.jsp" %>
