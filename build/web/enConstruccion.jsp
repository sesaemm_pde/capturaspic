<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@include file="encabezado.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>
                <a>Sitio en Construcci&oacute;n</a>
            </h1>
            <hr class="hrheader"/>
            <p>La p&aacute;gina solicitada se encuentra en construcci&oacute;n, disculpe las molestias.</p>
            <div class="grid-x grid-margin-x">
                <div class="cell medium-6 large-8">
                    <figure>
                        <img src="framework/img/svg/enconstruccion.svg" alt="Error"/>
                    </figure>
                </div>
                <div class="cell medium-6 large-4">
                    <h3>Otras opciones:</h3>
                    <hr class="hrheader"/>
                    <ul>
                        <li>
                            <a onclick="document.frmPrincipal.submit()">Regresar al inicio</a>
                        </li>
                        <li>
                            <a href="//plataformadigitalnacional.org/" target="_blank">Buscar en la Plataforma Digital Nacional</a>
                        </li>
                        <li>
                            <a href="javascript:history.back();">Volver a la p&aacute;gina anterior</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="piePagina.jsp" %>
