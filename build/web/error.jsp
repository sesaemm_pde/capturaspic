<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@include file="encabezado.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>
                <a>Error</a>
            </h1>
            <h3>C&oacute;digo de error : ${codigo}</h3>
            <hr class="hrheader"/>
            <p>${mensaje}</p>
            <p>Toma una impresi&oacute;n de pantalla de esta p&aacute;gina y env&iacute;ala al correo electr&oacute;nico
                <a href="mailto:soporte@sesaemm.org.mx">soporte@sesaemm.org.mx</a>.</p>
            <p>Para imprimir pantalla en Windows presiona
                <kbd>Windows + Imp Pnt</kbd>
                en el teclado, la imagen se guardar&aacute; en tu carpeta de Imagenes, en la subcarpeta de Capturas de pantalla.</p>
            <p>En macOS Mojave, presiona
                <kbd>Shift + Command + 5</kbd>
                en el teclado para ver todos los controles necesarios a fin de capturar im&aacute;genes fijas.</p>
            <p>Si prefieres comun&iacute;cate con nosotros al tel&eacute;fono
                <a href="tel:017229146034;">(01722) 914 6034</a>.</p>
            <div class="grid-x grid-margin-x">
                <div class="cell medium-6 large-8">
                    <figure>
                        <img src="framework/img/svg/bug1.svg" alt="Error"/>
                    </figure>
                </div>
                <div class="cell medium-6 large-4">
                    <h3>Otras opciones:</h3>
                    <hr class="hrheader"/>
                    <ul>
                        <li>
                            <a onclick="document.frmPrincipal.submit()">Regresar al inicio</a>
                        </li>
                        <li>
                            <a href="//plataformadigitalnacional.org/" target="_blank">Buscar en la Plataforma Digital Nacional</a>
                        </li>
                        <li>
                            <a href="javascript:history.back();">Volver a la p&aacute;gina anterior</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="piePagina.jsp" %>
