<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<div id="migas" class="hide-for-print">
    <div class="grid-container">
        <nav aria-label="Usted est&aacute; aqu&iacute;:" role="navigation">
            <ul class="breadcrumbs padding-_5">
                <c:forEach items="${migas}" var="miga">
                    <li>
                        <c:choose>
                            <c:when test="${miga.vinculoMiga == ''}">
                                <b>${miga.valorMiga}</b>
                            </c:when>
                            <c:otherwise>
                                <a onclick="frm${miga.vinculoMiga}.submit()">${miga.valorMiga}</a>
                            </c:otherwise>
                        </c:choose>

                    </li>
                </c:forEach>
            </ul>
        </nav>
    </div>
</div>
<c:forEach items="${migas}" var="miga">
    <c:if test="${miga.vinculoMiga != ''}">
        <form name="frm${miga.vinculoMiga}" id="frm${miga.vinculoMiga}" method="post" action="/sistemaCaptura${initParam.versionIntranet}/${miga.contexto}">
            <input type="hidden" name="accion" value="${miga.vinculoMiga}"/>
        </form>
    </c:if>
</c:forEach>
