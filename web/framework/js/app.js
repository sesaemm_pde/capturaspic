Foundation.Abide.defaults.patterns['curp'] = /^[A-Za-z]{1}[AEIOUaeiou]{1}[A-Za-z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HMhm]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE|as|bc|bs|cc|cs|ch|cl|cm|df|dg|gt|gr|hg|jc|mc|mn|ms|nt|nl|oc|pl|qt|qr|sp|sl|sr|tc|ts|tl|vz|yn|zs|ne)[B-DF-HJ-NP-TV-Zb-df-hj-np-tv-z]{3}[0-9A-Za-z]{1}[0-9]{1}$/; //Patron personalizado para CURP

Foundation.Abide.defaults.patterns['rfc_pf'] = /^[A-Za-z]{1}[AEIOUaeiou]{1}[A-Za-z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Za-z0-9]{3}$|^[A-Za-z]{1}[AEIOUaeiou]{1}[A-Za-z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])$/; //Patron personalizado para RFC de persona fisica

Foundation.Abide.defaults.patterns['rfc_pm'] = /^[A-Za-z]{3}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Za-z0-9]{3}$/; //Patron personalizado para RFC de persona moral

Foundation.Abide.defaults.patterns['year'] = /^(198|199|200|201|202|203|204|205|206|207)\d{1}$/; //Patron personalizado para A&ntilde;o Fiscal de 1980 a 2079

Foundation.Abide.defaults.patterns['url'] = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/; //Patron para validar url

Foundation.Abide.defaults.patterns['codigo_postal'] = /^((?!(0))[0-9]{5})$/;

Foundation.Abide.defaults.patterns['telefono'] = /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;

Foundation.Abide.defaults.patterns['solo_enteros'] = /^\d+$/;

Foundation.Abide.defaults.patterns['fecha'] = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;

$(document).foundation(); 

setTimeout(function ()
    {
        $('a[target="_blank"]').each(function ()
            {
                var a = $(this).html();
                0 == a.includes('<i class') && $(this).append('<i class="material-icons md-1t">exit_to_app</i>')
            })
    }, 1e3), window.setInterval(function ()
    {
        $('a[target="_blank"]').each(function ()
            {
                var a = $(this).html();
                0 == a.includes('<i class') && $(this).append('<i class="material-icons md-1t">exit_to_app</i>')
            })
    }, 1e3);


function reiniciarValoresS2()
{ 
    document.getElementById('txtNombre').setAttribute('value', '');
    document.getElementById('txtPrimerApellido').setAttribute('value', '');
    document.getElementById('txtInstitucion').setAttribute('value', '');
} 

function consultarServidoresPublicosS2()
{
    var form = document.createElement("form");
        form.method = "POST";
        form.action = document.getElementById("frmFiltroContrataciones").action;

        var accion = document.createElement("input");
        accion.value = 'consultarServidoresPublicosEnContrataciones';
        accion.name = "accion";
        accion.type = "hidden";
        form.appendChild(accion);

        var cmbPaginacion = document.createElement("input");
        cmbPaginacion.value = document.getElementById("cmbPaginacion").value;
        cmbPaginacion.name = "cmbPaginacion";
        cmbPaginacion.type = "hidden";
        form.appendChild(cmbPaginacion);

        var cmbOrdenacion = document.createElement("input");
        cmbOrdenacion.value = document.getElementById("cmbOrdenacion").value;
        cmbOrdenacion.name = "cmbOrdenacion";
        cmbOrdenacion.type = "hidden";
        form.appendChild(cmbOrdenacion);

        var txtNombre = document.createElement("input");
        txtNombre.value = document.getElementById("txtNombre").value;
        txtNombre.name = "txtNombre";
        txtNombre.type = "hidden";
        form.appendChild(txtNombre);

        var txtPrimerApellido = document.createElement("input");
        txtPrimerApellido.value = document.getElementById("txtPrimerApellido").value;
        txtPrimerApellido.name = "txtPrimerApellido";
        txtPrimerApellido.type = "hidden";
        form.appendChild(txtPrimerApellido);

        var txtInstitucion = document.createElement("input");
        txtInstitucion.value = document.getElementById("txtInstitucion").value;
        txtInstitucion.name = "txtInstitucion";
        txtInstitucion.type = "hidden";
        form.appendChild(txtInstitucion);

        document.body.appendChild(form);
        form.submit();
}

function existeUrl(url)
{
    $.get(url)
            .done(function ()
                {
                    return true;
                })
            .fail(function ()
                {
                    var form = document.createElement("form");
                    form.method = "POST";
                    form.action = "/pde";

                    var accion = document.createElement("input");
                    accion.value = 'noFound';
                    accion.name = "accion";
                    accion.type = "hidden";
                    form.appendChild(accion);

                    document.body.appendChild(form);
                    form.submit();
                    return false;
                });

}

function confirmar(id, mensaje) {

  swal({
      title: "Confirmar",
      text: mensaje,
      icon: "warning",
      buttons: ["Cancelar", "Aceptar"],
      dangerMode: true,
      closeOnClickOutside: false,
      closeOnEsc: false,
    })
    .then((seAcepta) => {
      if (seAcepta) {
          mostrarLoader();
        document.getElementById(id).submit();
      } else {
        swal("Se ha cancelado la operaci\u00f3n", {
          icon: "error",
          buttons: false,
          timer: 2000,
        });
      }
    });
}

function mostrarLoader() {
    document.getElementById("cargaDB").style.display = "block"; 
}