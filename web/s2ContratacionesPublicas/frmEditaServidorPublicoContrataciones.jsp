<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri = "http://saemm.gob.mx" prefix="patterns"%>

<%@include file="/encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-sis-min.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="/migas.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>Plataforma Digital Estatal</h1>
            <h2>Sistema II</h2>
            <h4>Servidores p&uacute;blicos que intervengan en procedimientos de contrataciones p&uacute;blicas.</h4>
            <div id="cargaDB" class="reveal-overlay" style="display: none;">
                <div id="loader"></div>
                <div id="textDB">
                    Actualizando base de datos...
                </div>
            </div>
            <div class="stacked-for-small expanded large button-group">
                <a class="button hollow" onclick="document.frmListado.submit()"><i class="material-icons md-4t">list</i> Listado de servidores p&uacute;blicos</a>
                <a class="button hollow" onclick="document.frmNuevo.submit()"><i class="material-icons md-4t">playlist_add</i> Agregar nuevo servidor p&uacute;blico</a>
                <form name="frmNuevo" method="post" action="/sistemaCaptura${initParam.versionIntranet}/contrataciones">
                    <input type="hidden" name="accion" value="mostrarPaginaRegistroNuevaContrataciones"/>
                </form>
            </div>
            <form name="frmListado" method="post" action="/sistemaCaptura${initParam.versionIntranet}/contrataciones">
                <input type="hidden" name="accion" value="ingresarSistema"/>
            </form>
            <h2>Modificar servidor p&uacute;blico</h2>
            <p><span style="color:red;">*</span> Campo obligatorio</p>
            <form name="frmRegistrar" id="frmRegistrar" method="post" action="/sistemaCaptura${initParam.versionIntranet}/contrataciones" data-abide novalidate onSubmit="event.preventDefault(); validarSeleccionChecks(this.id);" autocomplete="off">
                <input type="hidden" name="accion" value="editarServidorPublicoContrataciones"/>
                <input type="hidden" name="txtId" value="${servidorPublico.idSpdn}"/>
                <input type="hidden" name="txtNombre" value="${filtro.NOMBRES}"/>
                <input type="hidden" name="txtPrimerApellido" value="${filtro.PRIMER_APELLIDO}"/>
                <input type="hidden" name="txtOficina" value="${filtro.INSTITUCION_DEPENDENCIA}"/>
                <input type="hidden" name="cmbPaginacion" value="${filtro.registrosMostrar}"/>
                <input type="hidden" name="cmbOrdenacion" value="${filtro.orden}"/>
                <input type="hidden" name="txtNumeroPagina" value="${filtro.numeroPagina}"/>
                <div data-abide-error="data-abide-error" class="alert callout alertFooter" style="display: none;">
                    <p>
                        <i class="material-icons">warning</i>
                        Existen algunos errores en tu registro. Verifica nuevamente.
                    </p>
                </div>
                <fieldset class="fieldset">
                    <legend>Servidor p&uacute;blico</legend>
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">Ejercicio fiscal
                            <input type="number" min="1980" max="2079" name="txtEJERCICIO_FISCAL" id="txtEJERCICIO_FISCAL" placeholder="p. ej. 2018" pattern="year" value="${servidorPublico.ejercicioFiscal}"/>
                            <span class="form-error" data-form-error-for="txtEJERCICIO_FISCAL">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Nombres <span style="color:red;">*</span>
                            <input type="text" name="txtNOMBRES" id="txtNOMBRES" placeholder="p. ej. Carlos" required value="${servidorPublico.nombres}"/>
                            <span class="form-error" data-form-error-for="txtNOMBRES">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Primer apellido <span style="color:red;">*</span>
                            <input type="text" name="txtPRIMER_APELLIDO" id="txtPRIMER_APELLIDO" placeholder="p. ej. P&eacute;rez" required value="${servidorPublico.primerApellido}"/>
                            <span class="form-error" data-form-error-for="txtPRIMER_APELLIDO">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Segundo apellido
                            <input type="text" name="txtSEGUNDO_APELLIDO" id="txtSEGUNDO_APELLIDO" placeholder="p. ej. P&eacute;rez" value="${servidorPublico.segundoApellido}"/>
                            <span class="form-error" data-form-error-for="txtSEGUNDO_APELLIDO">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">RFC
                            <input type="text" name="txtRFC" id="txtRFC" class="mayusculas" placeholder="p. ej. XAXX010101XX0 o XAXX010101" pattern="rfc_pf" value="${patterns:validarRfcPF(servidorPublico.rfc) ? servidorPublico.rfc : ""}"/>
                            <span class="form-error" data-form-error-for="txtRFC">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">CURP
                            <input type="text" name="txtCURP" id="txtCURP" class="mayusculas" placeholder="p. ej. XAXX010101XAXAXA01" pattern="curp" value="${patterns:validarCurp(servidorPublico.curp) ? servidorPublico.curp : ""}"/>
                            <span class="form-error" data-form-error-for="txtCURP">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="cell medium-4">
                            <legend>G&eacute;nero</legend>
                            <c:forEach items="${generos}" var="genero">
                                <label>
                                    <input type="radio" name="radGENERO" id="radGENERO${genero.clave}" value="${genero.clave}" ${servidorPublico.genero.clave == genero.clave ? 'checked': ''}/>
                                    <label for="radGENERO${genero.clave}">${genero.valor}</label>
                                </label>
                            </c:forEach>
                        </fieldset>
                        <label class="cell medium-4">Ramo
                            <select name="cmbRAMO" id="cmbRAMO">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${ramos}" var="ramo">
                                    <option value="${ramo.clave}" ${servidorPublico.ramo.clave == ramo.clave ? 'selected': ''}>${ramo.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" id="spanRAMO">
                                Campo requerido, elige una opci&oacute;n de la lista.
                            </span>
                        </label>
                        <label class="cell medium-4">Ente p&uacute;blico <span style="color:red;">*</span>
                            <select name="cmbDEPENDENCIA" id="cmbDEPENDENCIA" required>
                                <option value="" disabled="disabled" selected="selected">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${entesPublicos}" var="ente_publico">
                                    <option value="${ente_publico.clave}" ${servidorPublico.institucionDependencia.clave == ente_publico.clave ? 'selected' : ''}>${ente_publico.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbDEPENDENCIA">
                                Campo requerido, elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">Puesto <span style="color:red;">*</span>
                            <select name="cmbPUESTO" id="cmbPUESTO" required>
                                <option value="" disabled="disabled" selected="selected">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${puestos}" var="puesto">
                                    <option value="${puesto.id}" ${servidorPublico.puesto.nombre == puesto.funcional ? 'selected' : '' }>${puesto.funcional}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbPUESTO">
                                Campo requerido, elige una opci&oacute;n.
                            </span>
                        </label>
                        <fieldset class="cell medium-4">
                            <legend>Tipo de &aacute;rea</legend>
                            <c:forEach items="${tipoDeArea}" var="area">
                                <c:set var="found" value="false" scope="request" />
                                <c:forEach items="${servidorPublico.tipoArea}" var="area_sp">
                                    <c:if test="${area_sp.clave == area.clave}">
                                        <c:set var="found" value="true" scope="request" />
                                    </c:if>
                                </c:forEach>
                                <label class="text-truncate">
                                    <input type="checkbox" name="chckAREA" value="${area.clave}" ${found ? 'checked' : ''}   />
                                    ${area.valor}
                                </label>
                            </c:forEach>
                            <span class="form-error" id="spanAREA">
                                Campo requerido, selecciona al menos una opci&oacute;n.
                            </span>
                        </fieldset>

                        <fieldset class="cell medium-4">
                            <legend class="cell medium-4">Tipo de procedimiento <span style="color:red;">*</span></legend>
                            <c:forEach items="${tipoProcedimientos}" var="procedimiento">
                                <c:set var="found" value="false" scope="request" />
                                <c:forEach items="${servidorPublico.tipoProcedimiento}" var="tipoProcedimiento">
                                    <c:if test="${tipoProcedimiento.clave == procedimiento.clave}">
                                        <c:set var="found" value="true" scope="request" />
                                    </c:if>
                                </c:forEach>
                                <label class="text-truncate">
                                    <input id="id_TP${procedimiento.clave}" type="checkbox" name="chckTIPO_PROCEDIMIENTO" value="${procedimiento.clave}" ${found ? 'checked' : ''} />
                                    ${procedimiento.valor}
                                </label>
                            </c:forEach>
                            <span class="form-error" id="chckTIPO_PROCEDIMIENTO">
                                Campo requerido, elige una opci&oacute;n.
                            </span>
                        </fieldset>

                        <fieldset class="cell medium-4">
                            <legend>Nivel de responsabilidad <span style="color:red;">*</span></legend>
                            <c:forEach items="${nivelesDeResponsabilidad}" var="nivelResponsabilidad">
                                <c:set var="found" value="false" scope="request" />
                                <c:forEach items="${servidorPublico.nivelResponsabilidad}" var="nivel_responsabilidad_sp">
                                    <c:if test="${nivel_responsabilidad_sp.clave == nivelResponsabilidad.clave}">
                                        <c:set var="found" value="true" scope="request" />
                                    </c:if>
                                </c:forEach>
                                <label class="text-truncate">
                                    <input id="id_r${nivelResponsabilidad.clave}" type="checkbox" name="chckRESPONSABILIDAD" value="${nivelResponsabilidad.clave}" ${found ? 'checked' : ''}/>
                                    ${nivelResponsabilidad.valor}
                                </label>
                            </c:forEach>
                            <span class="form-error" id="chckRESPONSABILIDAD">
                                Campo requerido, selecciona al menos una opci&oacute;n.
                            </span>
                        </fieldset>

                    </div>
                </fieldset>
                <fieldset class="fieldset">
                    <legend>Superior inmediato</legend>
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">Nombres
                            <input type="text" name="txtNOMBRES_S" id="txtNOMBRES_S" placeholder="p. ej. Carlos" value="${servidorPublico.superiorInmediato.nombres}"/>
                            <span class="form-error" data-form-error-for="txtNOMBRES_S">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Primer apellido
                            <input type="text" name="txtPRIMER_APELLIDO_S" id="txtPRIMER_APELLIDO_S" placeholder="p. ej. P&eacute;rez" value="${servidorPublico.superiorInmediato.primerApellido}"/>
                            <span class="form-error" data-form-error-for="txtPRIMER_APELLIDO_S">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Segundo apellido
                            <input type="text" name="txtSEGUNDO_APELLIDO_S" id="txtSEGUNDO_APELLIDO_S" placeholder="p. ej. P&eacute;rez" value="${servidorPublico.superiorInmediato.segundoApellido}"/>
                            <span class="form-error" data-form-error-for="txtSEGUNDO_APELLIDO_S">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">RFC
                            <input type="text" name="txtRFC_S" id="txtRFC_S" class="mayusculas" placeholder="p. ej. XAXX010101XX0 o XAXX010101" pattern="rfc_pf" value="${servidorPublico.superiorInmediato.rfc}"/>
                            <span class="form-error" data-form-error-for="txtRFC_S">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">CURP
                            <input type="text" name="txtCURP_S" id="txtCURP_S" class="mayusculas" placeholder="p. ej. XAXX010101XAXAXA01" pattern="curp" value="${servidorPublico.superiorInmediato.curp}"/>
                            <span class="form-error" data-form-error-for="txtCURP_S">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Puesto
                            <select name="cmbPUESTO_S" id="cmbPUESTO_S">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${puestos}" var="puesto">
                                    <option value="${puesto.id}" ${servidorPublico.superiorInmediato.puesto.nombre == puesto.funcional ? 'selected' : ''}>${puesto.funcional}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="txtPUESTO_S">
                                Campo requerido, elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </fieldset>
                <fieldset class="fieldset">
                    <div class="grid-margin-x grid-x">
                        <label class="cell">Observaciones
                            <textarea type="text" name="txtOBSERVACIONES" id="txtOBSERVACIONES" placeholder="p. ej. Aclaraci&oacute;n u observaciones de ..." >${servidorPublico.observaciones}</textarea> 
                            <span class="form-error" data-form-error-for="txtOBSERVACIONES">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </fieldset>
                <div class="grid-x grid-margin-x">
                    <fieldset class="cell medium-6">
                        <input type="reset" class="button expanded secondary" name="btnReset" value="Restaurar"/>
                    </fieldset>
                    <fieldset class="cell medium-6">
                        <input type="submit" class="button expanded" name="btnRegistrar" value="Modificar datos"/>
                    </fieldset>
                </div>
                <div data-abide-error="data-abide-error" class="alert callout alertFooter" style="display: none;">
                    <p>
                        <i class="material-icons">warning</i>
                        Existen algunos errores en tu registro. Verifica nuevamente.
                    </p>
                </div>
            </form>
        </div>
    </div>
</div>

<%@include file="/piePagina.jsp" %>

<script>

    function validarDatalist()
    {
        var val = $("#cmbRAMO").val();
        var obj = $("#listRAMO").find("option[value='" + val + "']");

        if (obj != null && obj.length > 0)
        {
            $("#spanRAMO").css("display", "none");
            return true;
        } else
        {
            $("#spanRAMO").css("display", "block");
            return false;
        }
    }

    function validarSeleccionChecks(id)
    {
        $("#frmRegistrar").on("formvalid.zf.abide", function (ev, frm)
        {

            var completoArea = 0;

            var allChecksBoxesResp = document.querySelectorAll('input[name="chckRESPONSABILIDAD"]:checked');
            if (allChecksBoxesResp.length === 0)
            {
                $("#chckRESPONSABILIDAD").css("display", "block");
                $(".alertFooter").css("display", "block");
                completoArea++;
            } else
            {
                $("#chckRESPONSABILIDAD").css("display", "none");
            }

            var allChecksBoxesTIPOP = document.querySelectorAll('input[name="chckTIPO_PROCEDIMIENTO"]:checked');
            if (allChecksBoxesTIPOP.length === 0)
            {
                $("#chckTIPO_PROCEDIMIENTO").css("display", "block");
                completoArea++;
            } else
            {
                $("#chckTIPO_PROCEDIMIENTO").css("display", "none");
            }

            if (completoArea === 0)
            {
                swal({
                    title: "Confirmar",
                    text: "\u00bf Est\u00e1 seguro de modificar el registro?",
                    icon: "warning",
                    buttons: ["Cancelar", "Aceptar"],
                    dangerMode: true,
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                })
                        .then((seAcepta) => {
                            if (seAcepta)
                            {
                                document.getElementById(id).submit();

                            } else
                            {
                                swal("Se ha cancelado la operaci\u00f3n", {
                                    icon: "error",
                                    buttons: false,
                                    timer: 2000,
                                });
                            }
                        });
            } else
            {
                return false;
            }
        });

    }
</script>
