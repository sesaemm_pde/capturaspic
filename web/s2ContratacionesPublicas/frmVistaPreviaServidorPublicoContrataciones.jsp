<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://saemm.gob.mx" prefix="patterns"%>

<%@include file="/encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-sis-min.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="/migas.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>Plataforma Digital Estatal</h1>
            <h2>Sistema II</h2>
            <h4>Servidores p&uacute;blicos que intervengan en procedimientos de contrataciones p&uacute;blicas.</h4>
            <div id="cargaDB" class="reveal-overlay" style="display: none;">
                <div id="loader"></div>
                <div id="textDB">
                    Actualizando base de datos...
                </div>
            </div>
            <div class="stacked-for-small expanded large button-group">
                <a class="button hollow" onclick="document.frmListado.submit()"><i class="material-icons md-4t">list</i> Listado de servidores p&uacute;blicos</a>
                <a class="button"><i class="material-icons md-4t">playlist_add</i> Agregar nuevo servidor p&uacute;blico</a>
            </div>
            <h2>Datos del servidor p&uacute;blico </h2>
            <fieldset class="fieldset">
                <legend>Servidor p&uacute;blico</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>Ejercicio fiscal</b>
                        <label>${servidorPublico.ejercicioFiscal}</label>
                    </label>
                    <label class="cell medium-4"><b>Nombres</b>
                        <label>${servidorPublico.nombres}</label>
                    </label>
                    <label class="cell medium-4"><b>Primer apellido</b>
                        <label>${servidorPublico.primerApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>Segundo apellido</b>
                        <label>${servidorPublico.segundoApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>RFC</b>
                        <label>${patterns:validarRfcPF(servidorPublico.rfc) ? servidorPublico.rfc : ""}</label>
                    </label>
                    <label class="cell medium-4"><b>CURP</b>
                        <label>${patterns:validarCurp(servidorPublico.curp) ? servidorPublico.curp : ""}</label>
                    </label>
                    <label class="cell medium-4"><b>G&eacute;nero</b>
                        <label>${servidorPublico.genero.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Ramo</b>
                        <label>${servidorPublico.ramo.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Ente p&uacute;blico</b>
                        <label>${servidorPublico.institucionDependencia.nombre}</label>
                    </label>
                    <label class="cell medium-4"><b>Puesto</b>
                        <label>${servidorPublico.puesto.nombre}</label>
                    </label>
                    <label class="cell medium-4"><b>Tipo de &aacute;rea</b>
                        <ul>
                            <c:forEach items="${servidorPublico.tipoArea}" var="area" varStatus="contador">
                                <li><label>${area.valor}${!contador.last ? ',' : ''}</label></li>
                                    </c:forEach>
                        </ul>
                    </label>
                    <label class="cell medium-4"><b>Tipo de procedimiento</b>
                        <ul>
                            <c:forEach items="${servidorPublico.tipoProcedimiento}" var="tipoProcedimiento" varStatus="contador">
                                <li><label>${tipoProcedimiento.valor}${!contador.last ? ',' : ''}</label></li>
                                    </c:forEach>
                        </ul>
                    </label>
                    <label class="cell medium-4"><b>Nivel de responsabilidad</b>
                        <ul><c:forEach items="${servidorPublico.nivelResponsabilidad}" var="nivelResponsabilidad" varStatus="contador" >
                                <li><label>${nivelResponsabilidad.valor}${!contador.last ? ',' : ''}</label></li>
                            </c:forEach></ul>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Superior inmediato</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>Nombres</b>
                        <label>${servidorPublico.superiorInmediato.nombres}</label>
                    </label>
                    <label class="cell medium-4"><b>Primer apellido</b>
                        <label>${servidorPublico.superiorInmediato.primerApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>Segundo apellido</b>
                        <label>${servidorPublico.superiorInmediato.segundoApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>CURP</b>
                        <label>${servidorPublico.superiorInmediato.curp}</label>
                    </label>
                    <label class="cell medium-4"><b>RFC</b>
                        <label>${servidorPublico.superiorInmediato.rfc}</label>
                    </label>
                    <label class="cell medium-4"><b>Puesto</b>
                        <label>${servidorPublico.superiorInmediato.puesto.nombre}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Observaciones</legend>
                <label class="cell">
                    <label>${servidorPublico.observaciones}</label>
                </label>
            </fieldset>
            <div class="grid-x grid-margin-x">
                <fieldset class="cell medium-6">
                    <form name="frmRegistrar" method="post" action="/sistemaCaptura${initParam.versionIntranet}/contrataciones" data-abide novalidate>
                        <input type="hidden" name="accion" value="modificarDatosServidorPublicoContrataciones"/>
                        <input type="hidden" name="txtEJERCICIO_FISCAL" id="txtEJERCICIO_FISCAL" value ="${servidorPublico.ejercicioFiscal}"/>
                        <input type="hidden" name="txtNOMBRES" id="txtNOMBRES" value="${servidorPublico.nombres}"/>
                        <input type="hidden" name="txtPRIMER_APELLIDO" id="txtPRIMER_APELLIDO" value="${servidorPublico.primerApellido}"/>
                        <input type="hidden" name="txtSEGUNDO_APELLIDO" id="txtSEGUNDO_APELLIDO" value="${servidorPublico.segundoApellido}"/>
                        <input type="hidden" name="txtRFC" id="txtRFC"  value="${servidorPublico.rfc}"/>
                        <input type="hidden" name="txtCURP" id="txtCURP" value="${servidorPublico.curp}" />
                        <input type="hidden" name="radGENERO" id="radGENERO" value="${servidorPublico.genero.clave}"/>
                        <input type="hidden" name="cmbRAMO" id="cmbRAMO" value="${servidorPublico.ramo.clave}"/>
                        <input type="hidden" name="cmbDEPENDENCIA" id="cmbDEPENDENCIA" value="${servidorPublico.institucionDependencia.clave}"/>
                        <input type="hidden" name="cmbPUESTO" id="cmbPUESTO" value="${servidorPublico.puesto.identificador}"/>
                        <c:forEach items="${servidorPublico.tipoArea}" var="tipoArea">
                            <input type="hidden" name="chckAREA" id="chckAREA" value="${tipoArea.clave}"/>
                        </c:forEach>
                        <c:forEach items="${servidorPublico.tipoProcedimiento}" var="tipoProcedimiento">
                            <input type="hidden" name="chckTIPO_PROCEDIMIENTO" id="chckTIPO_PROCEDIMIENTO" value="${tipoProcedimiento.clave}"/>
                        </c:forEach>
                        <c:forEach items="${servidorPublico.nivelResponsabilidad}" var="nivelResponsabilidad">
                            <input type="hidden" name="chckRESPONSABILIDAD" id="chckRESPONSABILIDAD" value="${nivelResponsabilidad.clave}"/>
                        </c:forEach>
                        <input type="hidden" name="txtNOMBRES_S" id="txtNOMBRES_S" value="${servidorPublico.superiorInmediato.nombres}"/>
                        <input type="hidden" name="txtPRIMER_APELLIDO_S" id="txtPRIMER_APELLIDO_S" value="${servidorPublico.superiorInmediato.primerApellido}"/>
                        <input type="hidden" name="txtSEGUNDO_APELLIDO_S" id="txtSEGUNDO_APELLIDO_S"  value="${servidorPublico.superiorInmediato.segundoApellido}"/>
                        <input type="hidden" name="txtCURP_S" id="txtCURP_S" value="${servidorPublico.superiorInmediato.curp}"/>
                        <input type="hidden" name="txtRFC_S" id="txtRFC_S" value="${servidorPublico.superiorInmediato.rfc}"/>
                        <input type="hidden" name="cmbPUESTO_S" id="cmbPUESTO_S" value="${servidorPublico.superiorInmediato.puesto.identificador}"/>
                        <input type="hidden" name="txtOBSERVACIONES" id="txtOBSERVACIONES" value="${servidorPublico.observaciones}"/>
                        <input type="submit" class="button expanded secondary" name="btnReset" value="Modificar datos"/>
                    </form>

                </fieldset>
                <fieldset class="cell medium-6">
                    <form name="frmRegistrar" method="post" action="/sistemaCaptura${initParam.versionIntranet}/contrataciones" data-abide novalidate>
                        <input type="hidden" name="accion" value="registrarServidorPublicoContrataciones"/>
                        <input type="hidden" name="txtEJERCICIO_FISCAL" id="txtEJERCICIO_FISCAL" value ="${servidorPublico.ejercicioFiscal}"/>
                        <input type="hidden" name="txtNOMBRES" id="txtNOMBRES" value="${servidorPublico.nombres}"/>
                        <input type="hidden" name="txtPRIMER_APELLIDO" id="txtPRIMER_APELLIDO" value="${servidorPublico.primerApellido}"/>
                        <input type="hidden" name="txtSEGUNDO_APELLIDO" id="txtSEGUNDO_APELLIDO" value="${servidorPublico.segundoApellido}"/>
                        <input type="hidden" name="txtRFC" id="txtRFC"  value="${servidorPublico.rfc}"/>
                        <input type="hidden" name="txtCURP" id="txtCURP" value="${servidorPublico.curp}" />
                        <input type="hidden" name="radGENERO" id="radGENERO" value="${servidorPublico.genero.clave}"/>
                        <input type="hidden" name="cmbRAMO" id="cmbRAMO" value="${servidorPublico.ramo.clave}"/>
                        <input type="hidden" name="cmbDEPENDENCIA" id="cmbDEPENDENCIA" value="${servidorPublico.institucionDependencia.clave}"/>
                        <input type="hidden" name="cmbPUESTO" id="cmbPUESTO" value="${servidorPublico.puesto.identificador}"/>
                        <c:forEach items="${servidorPublico.tipoArea}" var="tipoArea">
                            <input type="hidden" name="chckAREA" id="chckAREA" value="${tipoArea.clave}"/>
                        </c:forEach>
                        <c:forEach items="${servidorPublico.tipoProcedimiento}" var="tipoProcedimiento">
                            <input type="hidden" name="chckTIPO_PROCEDIMIENTO" id="chckTIPO_PROCEDIMIENTO" value="${tipoProcedimiento.clave}"/>
                        </c:forEach>
                        <c:forEach items="${servidorPublico.nivelResponsabilidad}" var="nivelResponsabilidad">
                            <input type="hidden" name="chckRESPONSABILIDAD" id="chckRESPONSABILIDAD" value="${nivelResponsabilidad.clave}"/>
                        </c:forEach>
                        <input type="hidden" name="txtNOMBRES_S" id="txtNOMBRES_S" value="${servidorPublico.superiorInmediato.nombres}"/>
                        <input type="hidden" name="txtPRIMER_APELLIDO_S" id="txtPRIMER_APELLIDO_S" value="${servidorPublico.superiorInmediato.primerApellido}"/>
                        <input type="hidden" name="txtSEGUNDO_APELLIDO_S" id="txtSEGUNDO_APELLIDO_S"  value="${servidorPublico.superiorInmediato.segundoApellido}"/>
                        <input type="hidden" name="txtCURP_S" id="txtCURP_S" value="${servidorPublico.superiorInmediato.curp}"/>
                        <input type="hidden" name="txtRFC_S" id="txtRFC_S" value="${servidorPublico.superiorInmediato.rfc}"/>
                        <input type="hidden" name="cmbPUESTO_S" id="cmbPUESTO_S" value="${servidorPublico.superiorInmediato.puesto.identificador}"/>
                        <input type="hidden" name="txtOBSERVACIONES" id="txtOBSERVACIONES" value="${servidorPublico.observaciones}"/>
                        <input type="submit" class="button expanded" name="btnRegistrar" value="Registrar"/>
                    </form>

                </fieldset>
            </div>
        </div>
    </div>
</div>
<form name="frmListado" method="post" action="/sistemaCaptura${initParam.versionIntranet}/contrataciones">
    <input type="hidden" name="accion" value="ingresarSistema"/>
</form>

<%@include file="/piePagina.jsp" %>
