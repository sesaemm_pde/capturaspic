package tol.sesaemm.funciones;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.integracion.DAOBase;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx 
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */

public class BitacoraPde
{

    public static String spic = "SPIC";
    public static String ssancionados = "SS";
    public static String psancionados = "PS";

    public static String registro = "registro";
    public static String modificacion = "modificacion";
    public static String borrado = "borrado";
    public static String publicacion = "publicacion";
    public static String despublicacion = "despublicacion";

    /**
     *
     * @param bitacora
     *
     * @return
     */
    public static boolean registrarBitacora(TBITACORAS_PDE bitacora) throws Exception
    {
        MongoClient conectarBaseDatosBusBus;
        MongoDatabase database;
        MongoCollection<TBITACORAS_PDE> collection;
        boolean blnSeRegistro = false;
        ConfVariables confVariables;

        confVariables = new ConfVariables();

        try
        {
            conectarBaseDatosBusBus = DAOBase.conectarBaseDatos();

            if (conectarBaseDatosBusBus != null)
            {
                database = conectarBaseDatosBusBus.getDatabase(confVariables.getBD());
                collection = database.getCollection("tbitacoras_pde", TBITACORAS_PDE.class);
                collection.insertOne(bitacora);
                conectarBaseDatosBusBus.close();
                blnSeRegistro = true;
            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al registrar bitacora de la PDE: " + ex.toString());
        }
        return blnSeRegistro;
    }
}
