/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.logicaDeNegocio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.bson.Document;
import tol.sesaemm.funciones.ManejoDeFechas;
import tol.sesaemm.funciones.ObtenerRfc;
import tol.sesaemm.ing.jidv.integracion.DAOContrataciones;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocioS2;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s2contrataciones.FILTRO_BUSQUEDA_SPIC;
import tol.sesaemm.ing.jidv.javabeans.s2contrataciones.SpicConPaginacion;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.GENERO;
import tol.sesaemm.javabeans.METADATA;
import tol.sesaemm.javabeans.METADATOS;
import tol.sesaemm.javabeans.PUESTO;
import tol.sesaemm.javabeans.RAMO;
import tol.sesaemm.javabeans.s2spic.NIVEL_RESPONSABILIDAD;
import tol.sesaemm.javabeans.s2spic.PUESTO_SPIC;
import tol.sesaemm.javabeans.s2spic.RAMO_SPIC;
import tol.sesaemm.javabeans.s2spic.SPIC;
import tol.sesaemm.javabeans.s2spic.SUPERIOR_INMEDIATO;
import tol.sesaemm.javabeans.s2spic.TIPO_AREA;
import tol.sesaemm.javabeans.s2spic.TIPO_PROCEDIMIENTO;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */
 
public class LogicaDeNegocioS2V01 implements LogicaDeNegocioS2
{

    /**
     * Obtener listado de las dependencias que el usuario puede modificar o
     * consultar
     *
     * @param usuario Usuario en sesión
     *
     * @return ArrayList Listado de dependencias
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ENTE_PUBLICO> obtenerDependenciasDelUsuario(USUARIO usuario) throws Exception
    {
        ArrayList<ENTE_PUBLICO> dependencias = usuario.getDependencias();
        return dependencias;
    }

    /**
     * Obtener listado de servidores públicos
     *
     * @param usuario Usuario de consulta que esta en sesión
     * @param dependencias Dependencias que el usuario puede consultar o
     * modificar
     * @param filtroBusqueda Filtro de búsqueda
     * @param orden Orden de la búsqueda
     *
     * @return ArrayList Listado de servidores públicos que intervienen en
     * contrataciones
     *
     * @throws Exception
     */
    @Override
    public SpicConPaginacion obtenerListadoDeServidoresPublicosQueIntervienenContrataciones(USUARIO usuario, ArrayList<ENTE_PUBLICO> dependencias, FILTRO_BUSQUEDA_SPIC filtroBusqueda) throws Exception
    {
        return DAOContrataciones.obtenerListadoDeServidoresPublicosQueIntervienenContrataciones(usuario, dependencias, filtroBusqueda);
    }

    /**
     * Obtener listado de tipo de áreas
     *
     * @return ArrayList Listado de áreas
     *
     * @throws Exception
     */
    @Override
    public ArrayList<TIPO_AREA> obtenerTiposDeArea() throws Exception
    {
        return DAOContrataciones.obtenerTiposDeArea();
    }

    /**
     * Obtener listado de tipo de áreas
     *
     * @return ArrayList Listado de áreas
     *
     * @throws Exception
     */
    @Override
    public ArrayList<TIPO_AREA> obtenerTipoDeArea(ArrayList<String> claves) throws Exception
    {
        return DAOContrataciones.obtenerTipoDeArea(claves);
    }

    /**
     * Obtener niveles de responsabilidad
     *
     * @return ArrayList Listado de niveles de responsabilidad
     *
     * @throws Exception
     */
    @Override
    public ArrayList<NIVEL_RESPONSABILIDAD> obtenerNivelesDeResponsabilidad() throws Exception
    {
        return DAOContrataciones.obtenerNivelesDeResponsabilidad();
    }

    /**
     * Obtener listado de puestos
     *
     * @return ArrayList Listado de puestos
     *
     * @throws Exception
     */
    @Override
    public ArrayList<PUESTO> obtenerPuestos() throws Exception
    {
        return DAOContrataciones.obtenerPuestos();
    }

    /**
     * *
     * Obtener el tipo de procedimiento
     *
     * @return ArrayList Listado de procedimientos
     *
     * @throws Exception
     */
    @Override
    public ArrayList<TIPO_PROCEDIMIENTO> obtenerTipoProcedimientos() throws Exception
    {
        return DAOContrataciones.obtenerTipoProcedimientos();
    }

    /**
     * Obtener listado de entes publicos
     *
     * @param dependencias Listado de dependecias asignados al usuario
     *
     * @return ArrayList Listadi de entes públicos
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ENTE_PUBLICO> obtenerEntesPublicos(ArrayList<ENTE_PUBLICO> dependencias, USUARIO usuario) throws Exception
    {
        return DAOContrataciones.obtenerEntesPublicos(dependencias, usuario);
    }

    /**
     * Obtener listado de ramos
     *
     * @return ArrayList Listado de ramos
     *
     * @throws Exception
     */
    @Override
    public ArrayList<RAMO> obtenerRamos() throws Exception
    {
        return DAOContrataciones.obtenerRamos();
    }

    /**
     * Modelar request para el registro de servidores públicos que intervienen
     * en contrataciones
     *
     * @param request Datos extra&iacute;dos del formulario
     *
     * @return SPIC Datos modelados
     *
     * @throws java.lang.Exception
     *
     */
    @Override
    public SPIC modelarRequest(HttpServletRequest request) throws Exception
    {
        SPIC servidor_publico;
        ArrayList<String> cod_tipo_area;
        ArrayList<String> nivelesResponsabilidad;
        ArrayList<String> cod_nivel_responsabilidad;
        SUPERIOR_INMEDIATO superior_inmediato;
        ArrayList<String> cod_tipoProcedimiento;
        ArrayList<Integer> tiposProcedimiento;

        servidor_publico = new SPIC();
        superior_inmediato = new SUPERIOR_INMEDIATO();
        nivelesResponsabilidad = new ArrayList<>();
        tiposProcedimiento = new ArrayList<>();

        servidor_publico.setEjercicioFiscal((request.getParameter("txtEJERCICIO_FISCAL") == null || request.getParameter("txtEJERCICIO_FISCAL").isEmpty()) ? null : request.getParameter("txtEJERCICIO_FISCAL"));

        servidor_publico.setNombres(request.getParameter("txtNOMBRES"));
        servidor_publico.setPrimerApellido(request.getParameter("txtPRIMER_APELLIDO"));
        servidor_publico.setSegundoApellido((request.getParameter("txtSEGUNDO_APELLIDO") == null || request.getParameter("txtSEGUNDO_APELLIDO").isEmpty()) ? null : request.getParameter("txtSEGUNDO_APELLIDO"));

        servidor_publico.setRfc((request.getParameter("txtRFC") != null && !request.getParameter("txtRFC").isEmpty()) ? request.getParameter("txtRFC").toUpperCase() : "");
        servidor_publico.setCurp((request.getParameter("txtCURP") != null && !request.getParameter("txtCURP").isEmpty()) ? request.getParameter("txtCURP").toUpperCase() : "");

        if (servidor_publico.getRfc() == null || servidor_publico.getRfc().isEmpty())
        {
            servidor_publico.setRfc(ObtenerRfc.obntenerRfcSPDN(servidor_publico.getNombres(), servidor_publico.getPrimerApellido(), servidor_publico.getSegundoApellido()).toUpperCase());
        }
        if (servidor_publico.getCurp() == null || servidor_publico.getCurp().isEmpty())
        {
            servidor_publico.setCurp(ObtenerRfc.obntenerRfcSPDN(servidor_publico.getNombres(), servidor_publico.getPrimerApellido(), servidor_publico.getSegundoApellido()).toUpperCase());
        }

        servidor_publico.setGenero((request.getParameter("radGENERO") == null || request.getParameter("radGENERO").isEmpty()) ? new GENERO() : DAOContrataciones.obtenerGenero(request.getParameter("radGENERO")));

        servidor_publico.setRamo((request.getParameter("cmbRAMO") == null || request.getParameter("cmbRAMO").isEmpty()) ? new RAMO_SPIC() : DAOContrataciones.obtenerRamos(request.getParameter("cmbRAMO")));

        servidor_publico.setInstitucionDependencia(DAOContrataciones.obtenerDependencia(request.getParameter("cmbDEPENDENCIA")));

        servidor_publico.setPuesto(DAOContrataciones.obtenerPuesto(request.getParameter("cmbPUESTO")));

        if (request.getParameterValues("chckAREA") == null || request.getParameterValues("chckAREA").length == 0)
        {
            servidor_publico.setTipoArea(new ArrayList<>());
        }
        else
        {
            cod_tipo_area = new ArrayList(Arrays.asList(request.getParameterValues("chckAREA")));
            servidor_publico.setTipoArea(DAOContrataciones.obtenerTipoDeArea(cod_tipo_area));
        }

        cod_tipoProcedimiento = new ArrayList(Arrays.asList(request.getParameterValues("chckTIPO_PROCEDIMIENTO")));
        for (String codigo : cod_tipoProcedimiento)
        {
            if (codigo != null)
            {
                tiposProcedimiento.add(Integer.parseInt(codigo));
            }
        }
        servidor_publico.setTipoProcedimiento(DAOContrataciones.obtenerTipoProcedimientos(tiposProcedimiento));

        cod_nivel_responsabilidad = new ArrayList(Arrays.asList(request.getParameterValues("chckRESPONSABILIDAD")));
        for (String codigo : cod_nivel_responsabilidad)
        {
            if (codigo != null)
            {
                nivelesResponsabilidad.add(codigo);
            }
        }
        servidor_publico.setNivelResponsabilidad(DAOContrataciones.obtenerNivelesDeResponsabilidad(nivelesResponsabilidad));

        superior_inmediato.setNombres((request.getParameter("txtNOMBRES_S") == null || request.getParameter("txtNOMBRES_S").isEmpty()) ? null : request.getParameter("txtNOMBRES_S"));
        superior_inmediato.setPrimerApellido((request.getParameter("txtPRIMER_APELLIDO_S") == null || request.getParameter("txtPRIMER_APELLIDO_S").isEmpty()) ? null : request.getParameter("txtPRIMER_APELLIDO_S"));
        superior_inmediato.setSegundoApellido((request.getParameter("txtSEGUNDO_APELLIDO_S") == null || request.getParameter("txtSEGUNDO_APELLIDO_S").isEmpty()) ? null : request.getParameter("txtSEGUNDO_APELLIDO_S"));
        superior_inmediato.setCurp((request.getParameter("txtCURP_S") == null || request.getParameter("txtCURP_S").isEmpty()) ? null : request.getParameter("txtCURP_S").toUpperCase());
        superior_inmediato.setRfc((request.getParameter("txtRFC_S") == null || request.getParameter("txtRFC_S").isEmpty()) ? null : request.getParameter("txtRFC_S").toUpperCase());

        superior_inmediato.setPuesto((request.getParameter("cmbPUESTO_S") == null || request.getParameter("cmbPUESTO_S").isEmpty()) ? new PUESTO_SPIC() : DAOContrataciones.obtenerPuesto(request.getParameter("cmbPUESTO_S")));
        servidor_publico.setSuperiorInmediato(superior_inmediato);

        servidor_publico.setObservaciones((request.getParameter("txtOBSERVACIONES") == null || request.getParameter("txtOBSERVACIONES").isEmpty()) ? null : request.getParameter("txtOBSERVACIONES"));

        return servidor_publico;
    }

    /**
     * Registrar servidor público que interviene en contrataciones
     *
     * @param servidorPublico Datos del servidor público
     * @param usuario Usuario que registra
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return boolean TRUE- Se guardo FALSE - No se guardo
     *
     * @throws Exception
     */
    @Override
    public String registrarServidorPublicoQueIntervieneContrataciones(SPIC servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;
        METADATA metadata;
        METADATOS metadatos;

        metadata = new METADATA();
        metadatos = new METADATOS();
        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        {

            metadata.setActualizacion(new ManejoDeFechas().getDateNowAsString());
            metadata.setInstitucion(usuario.getDependencia().getValor());
            metadata.setContacto(usuario.getCorreo_electronico());
            metadata.setPersonaContacto(usuario.getNombreCompleto());
            servidorPublico.setMetadata(metadata);

            servidorPublico.setId(UUID.randomUUID().toString());
            servidorPublico.setFechaCaptura(new ManejoDeFechas().getDateNowAsString());

            servidorPublico.setDependencia("SAEMM");
            metadatos.setUsuario_registro(usuario.getUsuario());
            metadatos.setFecha_registro(new ManejoDeFechas().getDateNowAsString());
            servidorPublico.setMetadatos(metadatos);
            servidorPublico.setPublicar("0");

            if (DAOContrataciones.registrarServidorPublicoQueIntervieneContrataciones(servidorPublico, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha registrado correctamente al servidor p\u00FAblico: " + servidorPublico.getNombres() + " " + servidorPublico.getPrimerApellido() + (servidorPublico.getSegundoApellido() == null ? "" : " " + servidorPublico.getSegundoApellido()));
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible regitrar los datos del servidor p\u00FAblico, intente de nuevo!");
            }
        }
        else
        {
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        }
        return mensaje.toJson();
    }

    /**
     * Verifica si es mujer o hombre de acuerdo al curp que proporcionó
     *
     * @param curp Curp del servidor público
     *
     * @return boolean Devuelve un valor verdadero si el CURP corresponde a una
     * mujer y false si corresponde a un hombre
     */
    @Override
    public boolean esMujer(String curp)
    {
        if (curp != null && !curp.isEmpty())
        {

            if (curp.charAt(10) == 'M')
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        else
        {
            return false;
        }
    }

    /**
     * Buscar servidor publico que interviene en contrataciones
     *
     * @param idServidorPublico Identificador del servidor público
     *
     * @return SPIC Ficha de datos del servidor público
     *
     * @throws Exception
     */
    @Override
    public SPIC obtenerDetalleServidorPublicoIntervieneContrataciones(String idServidorPublico) throws Exception
    {
        return DAOContrataciones.obtenerDetalleServidorePublicoIntervienenContrataciones(idServidorPublico);
    }

    /**
     * Obtener el estatus de privacidad de los campos para el módulo SPIC
     *
     * @return SPIC Detalle de privacidad de los datos
     *
     * @throws Exception
     */
    @Override
    public SPIC obtenerEstatusDePrivacidadCamposSPIC() throws Exception
    {
        return DAOContrataciones.obtenerEstatusDePrivacidadCamposSPIC("contrataciones");
    }

    /**
     * Eliminar servidor público que interviene en contrataciones
     *
     * @param id Identificador del servidor público
     * @param usuario Usuario que borra
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     * @param servidorPublico
     *
     * @return boolean Devuelve un valor verdadero si se ha borrado el registro
     * de la base de datos
     *
     * @throws Exception
     */
    @Override
    public String eliminarServidorPublicoQueIntervieneContrataciones(String id, USUARIO usuario, NIVEL_ACCESO nivelAcceso, SPIC servidorPublico) throws Exception
    {
        Document mensaje;

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        {
            if (DAOContrataciones.eliminarServidorPublicoQueIntervieneContrataciones(id, usuario, nivelAcceso, servidorPublico))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha eliminado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible eliminar el registro!");
            }
        }
        else
        {
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        }

        return mensaje.toJson();
    }

    /**
     * Editar servidor público que interviene en contrataciones
     *
     * @param id Identificador del servidor público
     * @param servidorPublico
     * @param usuario Usuario que edita
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return boolean Devuelve un valor verdadero si el registro fue editado
     *
     * @throws Exception
     */
    @Override
    public String editarServidorPublicoQueIntervieneContrataciones(String id, SPIC servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        {

            if (servidorPublico.getMetadatos() != null)
            {
                servidorPublico.getMetadatos().setUsuario_modificacion(usuario.getUsuario());
                servidorPublico.getMetadatos().setFecha_modificacion(new ManejoDeFechas().getDateNowAsString());
            }
            else
            {
                METADATOS metadatos = new METADATOS();
                metadatos.setUsuario_modificacion(usuario.getUsuario());
                metadatos.setFecha_modificacion(new ManejoDeFechas().getDateNowAsString());
                servidorPublico.setMetadatos(metadatos);
            }

            if (servidorPublico.getMetadata() != null)
            {
                servidorPublico.getMetadata().setActualizacion(new ManejoDeFechas().getDateNowAsString());
                servidorPublico.getMetadata().setInstitucion(usuario.getDependencia().getValor());
                servidorPublico.getMetadata().setContacto(usuario.getCorreo_electronico());
                servidorPublico.getMetadata().setPersonaContacto(usuario.getNombreCompleto());
            }
            else
            {
                METADATA metadata = new METADATA();
                metadata.setActualizacion(new ManejoDeFechas().getDateNowAsString());
                metadata.setInstitucion(usuario.getDependencia().getValor());
                metadata.setContacto(usuario.getCorreo_electronico());
                metadata.setPersonaContacto(usuario.getNombreCompleto());
                servidorPublico.setMetadata(metadata);
            }

            servidorPublico.setPublicar("0");

            if (DAOContrataciones.editarServidorPublicoQueIntervieneContrataciones(id, servidorPublico, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha modificado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible modificar el registro!");
            }
        }
        else
        {
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\\u00F3n!");
        }

        return mensaje.toJson();
    }

    /**
     * Modelar request para modificacion
     *
     * @param request Request del formulario
     * @param servidorPublico Datos del servidor público
     *
     * @return SPIC Servidor público modelado
     *
     * @throws Exception
     */
    @Override
    public SPIC modelarRequest(HttpServletRequest request, SPIC servidorPublico) throws Exception
    {
        ArrayList<String> cod_tipo_area;
        ArrayList<String> nivelesResponsabilidad;
        ArrayList<String> cod_nivel_responsabilidad;
        SUPERIOR_INMEDIATO superior_inmediato;
        ArrayList<String> cod_tipoProcedimiento;
        ArrayList<Integer> tiposProcedimiento;

        superior_inmediato = new SUPERIOR_INMEDIATO();
        nivelesResponsabilidad = new ArrayList<>();
        tiposProcedimiento = new ArrayList<>();

        servidorPublico.setEjercicioFiscal((request.getParameter("txtEJERCICIO_FISCAL") == null || request.getParameter("txtEJERCICIO_FISCAL").isEmpty()) ? null : request.getParameter("txtEJERCICIO_FISCAL"));

        servidorPublico.setNombres(request.getParameter("txtNOMBRES"));
        servidorPublico.setPrimerApellido(request.getParameter("txtPRIMER_APELLIDO"));
        servidorPublico.setSegundoApellido((request.getParameter("txtSEGUNDO_APELLIDO") == null || request.getParameter("txtSEGUNDO_APELLIDO").isEmpty()) ? null : request.getParameter("txtSEGUNDO_APELLIDO"));

        servidorPublico.setGenero((request.getParameter("radGENERO") == null || request.getParameter("radGENERO").isEmpty()) ? new GENERO() : DAOContrataciones.obtenerGenero(request.getParameter("radGENERO")));

        servidorPublico.setRfc((request.getParameter("txtRFC") != null && !request.getParameter("txtRFC").isEmpty()) ? request.getParameter("txtRFC").toUpperCase() : "");
        servidorPublico.setCurp((request.getParameter("txtCURP") != null && !request.getParameter("txtCURP").isEmpty()) ? request.getParameter("txtCURP").toUpperCase() : "");

        if (servidorPublico.getRfc() == null || servidorPublico.getRfc().isEmpty())
        {
            servidorPublico.setRfc(ObtenerRfc.obntenerRfcSPDN(servidorPublico.getNombres(), servidorPublico.getPrimerApellido(), servidorPublico.getSegundoApellido()).toUpperCase());
        }
        if (servidorPublico.getCurp() == null || servidorPublico.getCurp().isEmpty())
        {
            servidorPublico.setCurp(ObtenerRfc.obntenerRfcSPDN(servidorPublico.getNombres(), servidorPublico.getPrimerApellido(), servidorPublico.getSegundoApellido()).toUpperCase());
        }

        servidorPublico.setRamo((request.getParameter("cmbRAMO") == null || request.getParameter("cmbRAMO").isEmpty()) ? new RAMO_SPIC() : DAOContrataciones.obtenerRamos(request.getParameter("cmbRAMO")));

        servidorPublico.setInstitucionDependencia(DAOContrataciones.obtenerDependencia(request.getParameter("cmbDEPENDENCIA")));

        servidorPublico.setPuesto(DAOContrataciones.obtenerPuesto(request.getParameter("cmbPUESTO")));

        if (request.getParameterValues("chckAREA") == null || request.getParameterValues("chckAREA").length == 0)
        {
            servidorPublico.setTipoArea(new ArrayList<>());
        }
        else
        {
            cod_tipo_area = new ArrayList(Arrays.asList(request.getParameterValues("chckAREA")));
            servidorPublico.setTipoArea(DAOContrataciones.obtenerTipoDeArea(cod_tipo_area));
        }

        cod_tipoProcedimiento = new ArrayList(Arrays.asList(request.getParameterValues("chckTIPO_PROCEDIMIENTO")));
        for (String codigo : cod_tipoProcedimiento)
        {
            if (codigo != null)
            {
                tiposProcedimiento.add(Integer.parseInt(codigo));
            }
        }
        servidorPublico.setTipoProcedimiento(DAOContrataciones.obtenerTipoProcedimientos(tiposProcedimiento));

        cod_nivel_responsabilidad = new ArrayList(Arrays.asList(request.getParameterValues("chckRESPONSABILIDAD")));
        for (String codigo : cod_nivel_responsabilidad)
        {
            if (codigo != null)
            {
                nivelesResponsabilidad.add(codigo);
            }
        }
        servidorPublico.setNivelResponsabilidad(DAOContrataciones.obtenerNivelesDeResponsabilidad(nivelesResponsabilidad));

        superior_inmediato.setNombres((request.getParameter("txtNOMBRES_S") == null || request.getParameter("txtNOMBRES_S").isEmpty()) ? null : request.getParameter("txtNOMBRES_S"));
        superior_inmediato.setPrimerApellido((request.getParameter("txtPRIMER_APELLIDO_S") == null || request.getParameter("txtPRIMER_APELLIDO_S").isEmpty()) ? null : request.getParameter("txtPRIMER_APELLIDO_S"));
        superior_inmediato.setSegundoApellido((request.getParameter("txtSEGUNDO_APELLIDO_S") == null || request.getParameter("txtSEGUNDO_APELLIDO_S").isEmpty()) ? null : request.getParameter("txtSEGUNDO_APELLIDO_S"));
        superior_inmediato.setCurp((request.getParameter("txtCURP_S") == null || request.getParameter("txtCURP_S").isEmpty()) ? null : request.getParameter("txtCURP_S").toUpperCase());
        superior_inmediato.setRfc((request.getParameter("txtRFC_S") == null || request.getParameter("txtRFC_S").isEmpty()) ? null : request.getParameter("txtRFC_S").toUpperCase());

        superior_inmediato.setPuesto((request.getParameter("cmbPUESTO_S") == null || request.getParameter("cmbPUESTO_S").isEmpty()) ? new PUESTO_SPIC() : DAOContrataciones.obtenerPuesto(request.getParameter("cmbPUESTO_S")));
        servidorPublico.setSuperiorInmediato(superior_inmediato);

        servidorPublico.setObservaciones((request.getParameter("txtOBSERVACIONES") == null || request.getParameter("txtOBSERVACIONES").isEmpty()) ? null : request.getParameter("txtOBSERVACIONES"));

        return servidorPublico;
    }

    /**
     * Publica el registro del servidor publico para ser visualizado en la
     * plataforma digital
     *
     * @param id Identificador del servidor público
     * @param spic Datos del servidor público a publicar
     * @param usuario Usuario de publica el registro
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return boolean Devuelve un valor verdadero si se ha realizado la
     * actualización
     *
     * @throws Exception
     */
    @Override
    public String publicarServidorPublicoQueIntervieneContrataciones(String id, SPIC spic, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        {

            spic.getMetadatos().setUsuario_publicacion(usuario.getUsuario());
            spic.getMetadatos().setFecha_publicacion(new ManejoDeFechas().getDateNowAsString());
            spic.setPublicar("1");

            if (DAOContrataciones.publicarServidorPublicoQueIntervieneContrataciones(id, spic, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha publicado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible publicar el registro!");
            }
        }
        else
        {
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        }

        return mensaje.toJson();
    }

    /**
     * Modelado de datos para filtrado de busqueda
     *
     * @param request HttpServletRequest
     *
     * @return FILTRO_BUSQUEDA_SPIC FILTRO DE BUSQUEDA
     */
    @Override
    public FILTRO_BUSQUEDA_SPIC modelarFiltroSpic(HttpServletRequest request)
    {
        FILTRO_BUSQUEDA_SPIC filtro;
        int numeroPagina;
        int registrosMostrar;
        String orden;

        filtro = new FILTRO_BUSQUEDA_SPIC();

        registrosMostrar = (request.getParameter("cmbPaginacion") == null) ? 10 : Integer.parseInt(request.getParameter("cmbPaginacion"));
        numeroPagina = (request.getParameter("txtNumeroPagina") == null ? 1 : Integer.parseInt(request.getParameter("txtNumeroPagina")));
        orden = (request.getParameter("cmbOrdenacion") == null) ? "nombres" : request.getParameter("cmbOrdenacion");

        filtro.setID((request.getParameter("txtId") == null) ? "" : request.getParameter("txtId").trim());
        filtro.setNOMBRES((request.getParameter("txtNombre") == null) ? "" : request.getParameter("txtNombre").trim());
        filtro.setPRIMER_APELLIDO((request.getParameter("txtPrimerApellido") == null) ? "" : request.getParameter("txtPrimerApellido").trim());
        filtro.setSEGUNDO_APELLIDO((request.getParameter("txtSegundoApellido") == null) ? "" : request.getParameter("txtSegundoApellido").trim());
        filtro.setINSTITUCION_DEPENDENCIA((request.getParameter("txtInstitucion") == null) ? "" : request.getParameter("txtInstitucion").trim());
        filtro.setCURP((request.getParameter("txtCurp") == null) ? "" : request.getParameter("txtCurp").trim());
        filtro.setNumeroPagina(numeroPagina);
        filtro.setRegistrosMostrar(registrosMostrar);
        filtro.setOrden(orden);

        return filtro;
    }

    /**
     * Despublicar registro del servidor público, después de esto no se podrá
     * visualizar en la plataforma digital
     *
     * @param id Identificador del registro
     * @param spic Datos del servidor público a despublicar
     * @param usuario Usuario que despublica
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return boolean
     *
     * @throws java.lang.Exception
     */
    @Override
    public String despublicarServidorPublicoQueIntervieneContrataciones(String id, SPIC spic, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        {

            spic.getMetadatos().setUsuario_despublicacion(usuario.getUsuario());
            spic.getMetadatos().setFecha_despublicacion(new ManejoDeFechas().getDateNowAsString());
            spic.setPublicar("0");

            if (DAOContrataciones.despublicarServidorPublicoQueIntervieneContrataciones(id, spic, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha despublicado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible despublicar el registro!");
            }
        }
        else
        {
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        }

        return mensaje.toJson();
    }

    /**
     * Permite obtener el tipo de procedimientos
     *
     * @param tipo_procedimiento
     *
     * @return ArrayList Que contiene los tipos de procedimientos de la base de
     * datos
     *
     * @throws Exception
     */
    @Override
    public ArrayList<TIPO_PROCEDIMIENTO> obtenerTipoProcedimientos(ArrayList<Integer> tipo_procedimiento) throws Exception
    {
        return DAOContrataciones.obtenerTipoProcedimientos(tipo_procedimiento);
    }

    /**
     * Obtener listado de generos
     *
     * @return ArrayList Lista de genero
     *
     * @throws Exception
     */
    @Override
    public ArrayList<GENERO> obtenerGeneros() throws Exception
    {
        return DAOContrataciones.obtenerGeneros();
    }

    /**
     * Devuelve un genero en especifico usando como filtro de b&uacute;squeda el
     * c&oacute;digo del genero
     *
     * @param codigo c&oacute;digo del genero que se buscara
     *
     * @return GENERO
     */
    @Override
    public GENERO obtenerGenero(String codigo) throws Exception
    {
        return DAOContrataciones.obtenerGenero(codigo);
    }

    /**
     * Devuelve los niveles de responsabilidad usando como filtro las claves del
     * nivel de responsabilidad
     *
     * @param claves
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public ArrayList<NIVEL_RESPONSABILIDAD> obtenerNivelesDeResponsabilidad(ArrayList<String> claves) throws Exception
    {
        return DAOContrataciones.obtenerNivelesDeResponsabilidad(claves);
    }

}
