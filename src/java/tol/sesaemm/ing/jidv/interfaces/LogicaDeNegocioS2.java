/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.interfaces;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s2contrataciones.FILTRO_BUSQUEDA_SPIC;
import tol.sesaemm.ing.jidv.javabeans.s2contrataciones.SpicConPaginacion;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.GENERO;
import tol.sesaemm.javabeans.PUESTO;
import tol.sesaemm.javabeans.RAMO;
import tol.sesaemm.javabeans.s2spic.NIVEL_RESPONSABILIDAD;
import tol.sesaemm.javabeans.s2spic.SPIC;
import tol.sesaemm.javabeans.s2spic.TIPO_AREA;
import tol.sesaemm.javabeans.s2spic.TIPO_PROCEDIMIENTO;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */
 
public interface LogicaDeNegocioS2
{

    public ArrayList<TIPO_AREA> obtenerTiposDeArea() throws Exception;

    public ArrayList<TIPO_AREA> obtenerTipoDeArea(ArrayList<String> claves) throws Exception;

    public ArrayList<NIVEL_RESPONSABILIDAD> obtenerNivelesDeResponsabilidad() throws Exception;

    public ArrayList<NIVEL_RESPONSABILIDAD> obtenerNivelesDeResponsabilidad(ArrayList<String> claves) throws Exception;

    public ArrayList<PUESTO> obtenerPuestos() throws Exception;

    public ArrayList<TIPO_PROCEDIMIENTO> obtenerTipoProcedimientos() throws Exception;

    public ArrayList<TIPO_PROCEDIMIENTO> obtenerTipoProcedimientos(ArrayList<Integer> tipo_procedimiento) throws Exception;

    public ArrayList<ENTE_PUBLICO> obtenerEntesPublicos(ArrayList<ENTE_PUBLICO> dependencias, USUARIO usuario) throws Exception;

    public ArrayList<RAMO> obtenerRamos() throws Exception;

    public ArrayList<GENERO> obtenerGeneros() throws Exception;

    public GENERO obtenerGenero(String codigo) throws Exception;

    public ArrayList<ENTE_PUBLICO> obtenerDependenciasDelUsuario(USUARIO usuario) throws Exception;

    public SpicConPaginacion obtenerListadoDeServidoresPublicosQueIntervienenContrataciones(USUARIO usuario, ArrayList<ENTE_PUBLICO> dependencias, FILTRO_BUSQUEDA_SPIC filtroBusqueda) throws Exception;

    public SPIC modelarRequest(HttpServletRequest request) throws Exception;

    public String registrarServidorPublicoQueIntervieneContrataciones(SPIC servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

    public boolean esMujer(String curp);

    public SPIC obtenerEstatusDePrivacidadCamposSPIC() throws Exception;

    public SPIC obtenerDetalleServidorPublicoIntervieneContrataciones(String idServidorPublico) throws Exception;

    public String eliminarServidorPublicoQueIntervieneContrataciones(String id, USUARIO usuario, NIVEL_ACCESO nivelAcceso, SPIC servidorPublico) throws Exception;

    public String editarServidorPublicoQueIntervieneContrataciones(String id, SPIC servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

    public SPIC modelarRequest(HttpServletRequest request, SPIC servidorPublico) throws Exception;

    public String publicarServidorPublicoQueIntervieneContrataciones(String id, SPIC servidorPublicoIntervieneContrataciones, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

    public FILTRO_BUSQUEDA_SPIC modelarFiltroSpic(HttpServletRequest request);

    public String despublicarServidorPublicoQueIntervieneContrataciones(String id, SPIC servidorPublicoIntervieneContrataciones, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

}
