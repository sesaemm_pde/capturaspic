/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.integracion;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Collation;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import org.bson.types.ObjectId;
import tol.sesaemm.annotations.Exclude;
import tol.sesaemm.funciones.BitacoraPde;
import tol.sesaemm.funciones.ExpresionesRegulares;
import tol.sesaemm.funciones.ManejoDeFechas;
import tol.sesaemm.funciones.TBITACORAS_PDE;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.PAGINACION;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s2contrataciones.FILTRO_BUSQUEDA_SPIC;
import tol.sesaemm.ing.jidv.javabeans.s2contrataciones.SpicConPaginacion;
import tol.sesaemm.javabeans.*;
import tol.sesaemm.javabeans.s2spic.*;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
 
public class DAOContrataciones
{

    /**
     * Obtener el listado de servidores p&uacute;blicos que intervienen en
     * contrataciones
     *
     * @param usuario Usuario que realiza la b&uacute;squeda
     * @param dependencias Dependencias asignadas al usuario
     * @param filtroBusqueda Filtro de b&uacute;squeda
     *
     * @return Listado de servidores p&uacute;blicos registrados que cumplen con
     * la condici&oacute;n
     *
     * @throws Exception
     */
    public static SpicConPaginacion obtenerListadoDeServidoresPublicosQueIntervienenContrataciones(USUARIO usuario, ArrayList<ENTE_PUBLICO> dependencias, FILTRO_BUSQUEDA_SPIC filtroBusqueda) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        AggregateIterable<Document> aIServidoresPublicos;                       
        MongoCursor<Document> cursor = null;                                    
        ArrayList<SPIC> servidoresPublicos;                                     
        List<Document> where_dependencia;                                       
        Document or;                                                            
        List<Document> and;                                                     
        Document where;                                                         
        ArrayList<Document> query;                                              
        Document order_by;                                                      
        int nivelDeAcceso = 0;                                                  
        PAGINACION filtroPaginacion;                                            
        int totalRegFiltro = 0;                                                 
        int totalPaginas = 0;                                                   
        int saltos = 0;                                                         
        SpicConPaginacion scp;   
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                               
        scp = null;
        query = new ArrayList<>();
        filtroPaginacion = new PAGINACION();
        Collation collation = Collation.builder().locale("es").caseLevel(true).build();
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("spic");

                servidoresPublicos = new ArrayList<>();
                where_dependencia = new ArrayList<>();
                or = new Document();
                and = new ArrayList<>();
                where = new Document();

                for (SISTEMAS sistema : usuario.getSistemas())
                { 
                    if (sistema.getSistema().getId() == 2)
                    { 
                        switch (sistema.getNivel_acceso().getId())
                        { 
                            case 1:
                            case 2:
                            case 3:
                            case 5:
                                nivelDeAcceso = sistema.getNivel_acceso().getId();
                                break;
                            default:
                                throw new Exception("Error no se reconocen los permisos del usuario");
                        } 
                    } 
                } 

                order_by = new Document();
                if (filtroBusqueda.getOrden().equals("nombres"))
                { 
                    order_by.append("nombres", 1);
                } 
                else if (filtroBusqueda.getOrden().equals("dependencia"))
                { 
                    order_by.append("institucionDependencia.nombre", 1);
                } 
                else if (filtroBusqueda.getOrden().equals("puesto"))
                { 
                    order_by.append("puesto.nombre", 1);
                } 
                
                if (nivelDeAcceso == 2)
                { 
                    where.append("metadatos.usuario_registro", new Document("$eq", usuario.getUsuario()));
                } 
                else if (nivelDeAcceso == 3)
                { 
                    for (ENTE_PUBLICO dependencia : dependencias)
                    { 
                        where_dependencia.add(new Document("institucionDependencia.clave", dependencia.getClave()));
                    } 
                    or.append("$or", where_dependencia);
                    and.add(or);
                    and.add(new Document("publicar", new Document("$ne", "0")));
                } 
                else if (nivelDeAcceso == 5)
                { 
                    and.add(new Document("publicar", new Document("$ne", "0")));
                } 

                if (filtroBusqueda.getNOMBRES() != null && !filtroBusqueda.getNOMBRES().isEmpty())
                { 
                    where.append("nombres",
                            new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(filtroBusqueda.getNOMBRES().trim())).append("$options", "i"));
                } 
                if (filtroBusqueda.getPRIMER_APELLIDO() != null && !filtroBusqueda.getPRIMER_APELLIDO().isEmpty())
                { 
                    where.append("primerApellido",
                            new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(filtroBusqueda.getPRIMER_APELLIDO().trim())).append("$options", "i"));
                } 
                if (filtroBusqueda.getINSTITUCION_DEPENDENCIA() != null && !filtroBusqueda.getINSTITUCION_DEPENDENCIA().isEmpty())
                { 
                    where.append("institucionDependencia.nombre",
                            new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(filtroBusqueda.getINSTITUCION_DEPENDENCIA().trim())).append("$options", "i"));
                } 
                
                if (!where.isEmpty())
                {
                    and.add(where);
                }
                if (and.size() > 0)
                {
                    query.add(new Document("$match", new Document("$and", and)));
                }

                query.add(new Document("$count", "totalCount"));
                aIServidoresPublicos = collection.aggregate(query)
                        .collation(collation)
                        .allowDiskUse(true);
                cursor = aIServidoresPublicos.iterator();
                if (cursor.hasNext())
                { 
                    totalRegFiltro = cursor.next().get("totalCount", Integer.class);
                    totalPaginas = (int) Math.ceil((float) totalRegFiltro / filtroBusqueda.getRegistrosMostrar());

                    if (filtroBusqueda.getNumeroPagina() > 1)
                    { 
                        saltos = (filtroBusqueda.getNumeroPagina() - 1) * filtroBusqueda.getRegistrosMostrar();
                    } 

                    filtroPaginacion.setRegistrosMostrar(filtroBusqueda.getRegistrosMostrar());
                    filtroPaginacion.setNumeroPagina(filtroBusqueda.getNumeroPagina());
                    filtroPaginacion.setTotalPaginas((totalPaginas < 1) ? 1 : totalPaginas);
                    filtroPaginacion.setNumeroSaltos(saltos);
                    filtroPaginacion.setTotalRegistros(totalRegFiltro);
                    filtroPaginacion.setIsEmpty(false);
                } 
                for (int i = 0; i < query.size(); i++)
                {
                    if (query.get(i).containsKey("$count"))
                    {
                        query.remove(i);
                    }
                }
                
                query.add(new Document("$sort",
                        order_by
                ));

                if (filtroPaginacion.isEmpty() == false)
                { 
                    query.add(new Document("$skip", filtroPaginacion.getNumeroSaltos()));
                    query.add(new Document("$limit", filtroPaginacion.getRegistrosMostrar()));
                } 

                servidoresPublicos = collection.aggregate(query, SPIC.class).collation(collation).allowDiskUse(true).into(new ArrayList<>());

                scp = new SpicConPaginacion(filtroPaginacion, servidoresPublicos);

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener la lista de servidores p&uacute;blicos que interviene en contrataciones publicas: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return scp;
    }

    /**
     * Obtener el listado de tipos de área
     *
     * @return Listado de tipo de área
     *
     * @throws java.lang.Exception
     */
    public static ArrayList<TIPO_AREA> obtenerTiposDeArea() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        ArrayList<TIPO_AREA> listadoTipoDeArea;      
        ConfVariables confVariables;

        confVariables = new ConfVariables();                             

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_area");

                listadoTipoDeArea = collection.find(TIPO_AREA.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener tipo de área: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        return listadoTipoDeArea;
    }

    /**
     * Obtener niveles de responsabilidad
     *
     * @return Niveles de responsabilidad
     *
     * @throws Exception
     */
    public static ArrayList<NIVEL_RESPONSABILIDAD> obtenerNivelesDeResponsabilidad() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        ArrayList<NIVEL_RESPONSABILIDAD> listadoNivelResponsabilidad;   
        ConfVariables confVariables;

        confVariables = new ConfVariables();          

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("nivel_responsabilidad");

                listadoNivelResponsabilidad = collection.find(NIVEL_RESPONSABILIDAD.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener niveles de responsabilidad:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoNivelResponsabilidad;
    }

    /**
     * Obtener listado de puestos
     *
     * @return Listado de puestos del servidor p&uacute;blico
     *
     * @throws Exception
     */
    public static ArrayList<PUESTO> obtenerPuestos() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        FindIterable<Document> aIPuesto;                                        
        ArrayList<PUESTO> listadoPuestos;                                       
        PUESTO puesto;                                                          
        MongoCursor<Document> cursor = null;                                    
        JsonWriterSettings settings;                                            
        Gson gson;
        ConfVariables confVariables;

        confVariables = new ConfVariables();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("puesto");

                aIPuesto = collection.find().sort(new Document("funcional", 1));
                cursor = aIPuesto.iterator();

                listadoPuestos = new ArrayList<>();
                gson = new Gson();
                settings = JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build();

                while (cursor.hasNext())
                { 

                    Document next = cursor.next();
                    puesto = gson.fromJson(next.toJson(settings), PUESTO.class);
                    puesto.setNombre(puesto.getFuncional().trim());
                    puesto.setId(next.get("_id", ObjectId.class));
                    listadoPuestos.add(puesto);

                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener puestos funcionales:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return listadoPuestos;
    }

    /**
     * Obtener listado de tipos de procedimientos
     *
     * @return Listado de tipos de procedimientos
     *
     * @throws Exception
     */
    public static ArrayList<TIPO_PROCEDIMIENTO> obtenerTipoProcedimientos() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        ArrayList<TIPO_PROCEDIMIENTO> listadoProcedimientos;    
        ConfVariables confVariables;

        confVariables = new ConfVariables();                  
        listadoProcedimientos = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_procedimiento");

                listadoProcedimientos = collection.find(TIPO_PROCEDIMIENTO.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener tipo de procedimiento:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoProcedimientos;
    }

    /**
     * Obtener listado de entes p&uacute;blicos asignados al usuario, si es
     * administrador obtendrá toda la lista
     *
     * @param dependencias Lista de dependencias asignadas
     * @param usuario Usuario en sesi&oacute;n
     *
     * @return Lista de entes p&uacute;blicos asignados al usuario
     *
     * @throws Exception
     */
    public static ArrayList<ENTE_PUBLICO> obtenerEntesPublicos(ArrayList<ENTE_PUBLICO> dependencias, USUARIO usuario) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        ArrayList<ENTE_PUBLICO> listadoProcedimientos;                          
        Boolean esAdmin = false;        
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                          

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ente_publico");
                List<Document> where = new ArrayList<>();

                Document or = new Document();

                for (SISTEMAS sistema : usuario.getSistemas())
                { 
                    if (sistema.getSistema().getId() == 2)
                    { 
                        if (sistema.getNivel_acceso().getId() == 1)
                        { 
                            esAdmin = true;
                        } 
                    } 
                } 

                if (!esAdmin)
                { 
                    for (ENTE_PUBLICO dependencia : dependencias)
                    { 
                        where.add(new Document("clave", dependencia.getClave()));
                    } 
                    or.append("$or", where);
                } 

                listadoProcedimientos = collection.find(or, ENTE_PUBLICO.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener entes p&uacute;blicos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoProcedimientos;
    }

    /**
     * Obtener listado de ramos
     *
     * @return Listado de ramos
     *
     * @throws Exception
     */
    public static ArrayList<RAMO> obtenerRamos() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        ArrayList<RAMO> listadoRamo;     
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                         

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ramo");

                listadoRamo = collection.find(RAMO.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener ramos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoRamo;
    }

    /**
     * Obtener el listado de g&eacute;neros
     *
     * @return Listado de generos
     *
     * @throws Exception
     */
    public static ArrayList<GENERO> obtenerGeneros() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        ArrayList<GENERO> listadoGeneros;   
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                      

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("genero");

                listadoGeneros = collection.find(GENERO.class).sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener generos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoGeneros;
    }

    /**
     * Registrar servidor p&uacute;blico que intervienen en contrataciones
     *
     * @param servidorPublico Servidor p&uacute;blico
     * @param usuario Usuario de registro
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return boolean
     *
     * @throws java.lang.Exception
     */
    public static boolean registrarServidorPublicoQueIntervieneContrataciones(SPIC servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document spic;                                                          
        boolean seRegistra;     
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                                  

        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();

        seRegistra = false;
        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("spic");

                if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
                { 
                    spic = Document.parse(gson.toJson(servidorPublico));
                    collection.insertOne(spic);
                    
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.spic, "SAEMM", servidorPublico.getId(), spic.toJson(), BitacoraPde.registro));
                    

                    seRegistra = true;
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al registrar el servidor p&uacute;blico que interviene en contrataciones : " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        return seRegistra;
    }

    /**
     * Obtener ramo descripci&oacute;n del tipo de ramo
     *
     * @param clave Identificador del ramo
     *
     * @return Descripci&oacute;n del ramo
     *
     * @throws Exception
     */
    public static RAMO_SPIC obtenerRamos(String clave) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        Document where;                                                         
        RAMO ramo;                                                              
        RAMO_SPIC ramoSpic;     
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                                  

        ramoSpic = new RAMO_SPIC();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ramo");
                where = new Document();
                where.append("clave", clave);

                ramo = collection.find(where, RAMO.class).first();

                if (ramo != null)
                { 
                    ramoSpic.setClave(Integer.parseInt(ramo.getClave()));
                    ramoSpic.setValor(ramo.getValor());
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida ");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener ramo por id:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return ramoSpic;
    }

    /**
     *
     * Se obtienen datos de la dependencia
     *
     * @param clave_dependencia Clave de la dependencia a buscar
     *
     * @return Datos de la dependencia solicitada
     *
     * @throws Exception
     */
    public static INSTITUCION_DEPENDENCIA obtenerDependencia(String clave_dependencia) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ENTE_PUBLICO> collection;                               
        Document where;                                                         
        INSTITUCION_DEPENDENCIA institucion;                                    
        ENTE_PUBLICO entePublico;  
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                               

        institucion = new INSTITUCION_DEPENDENCIA();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ente_publico", ENTE_PUBLICO.class);
                where = new Document();
                where.append("clave", clave_dependencia);

                entePublico = collection.find(where).first();

                if (entePublico != null)
                { 
                    institucion.setClave(entePublico.getClave());
                    institucion.setNombre(entePublico.getValor());
                    institucion.setSiglas(entePublico.getSiglas());
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener instituci&oacute;n por id:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return institucion;
    }

    /**
     * Se obtienen datos del puesto solicitado por ID
     *
     * @param id Identificador del puesto de trabajo
     *
     * @return Datos del puesto de trabajo
     *
     * @throws Exception
     */
    public static PUESTO_SPIC obtenerPuesto(String id) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<PUESTO> collection;                                     
        Document where;                                                         
        PUESTO puesto;                                                          
        PUESTO_SPIC puestoSpic;     
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                              

        puestoSpic = new PUESTO_SPIC();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("puesto", PUESTO.class);
                where = new Document();
                where.append("_id", new ObjectId(id));

                puesto = collection.find(where).first();

                if (puesto != null)
                { 
                    puestoSpic.setIdentificador(puesto.getId().toString());
                    puestoSpic.setNivel(puesto.getNivel());
                    puestoSpic.setNombre(puesto.getFuncional());
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener puesto por id:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return puestoSpic;
    }

    /**
     * Obtener la descripci&oacute;n del procedimiento especificando el ID
     *
     * @param clave_procedimiento Id del procedimiento
     *
     * @return Descripci&oacute;n del procedimiento
     *
     * @throws Exception
     */
    public static String obtenerProcedimiento(String clave_procedimiento) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        FindIterable<Document> aIPuesto;                                        
        MongoCursor<Document> cursor = null;                                    
        Document where;                                                         
        String procedimiento = "";                                              
        TIPO_PROCEDIMIENTO tipo_procedimiento;
        JsonWriterSettings settings;                                            
        Gson gson;
        ConfVariables confVariables;

        confVariables = new ConfVariables();  

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_procedimiento");
                where = new Document();
                where.append("clave", clave_procedimiento);

                aIPuesto = collection.find(where);
                cursor = aIPuesto.iterator();
                gson = new Gson();
                settings = JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build();

                if (cursor.hasNext())
                { 
                    Document next = cursor.next();
                    tipo_procedimiento = gson.fromJson(next.toJson(settings), TIPO_PROCEDIMIENTO.class);
                    procedimiento = tipo_procedimiento.getValor();
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida ");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener procedimiento por id:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return procedimiento;
    }

    /**
     * Obtener el estatus de privacidad de los campos que se mostrarán
     *
     * @param coleccion
     *
     * @return
     *
     * @throws Exception
     */
    public static SPIC obtenerEstatusDePrivacidadCamposSPIC(String coleccion) throws Exception
    {
        SPIC spic;                                                              
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;   
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                  

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();
            spic = new SPIC();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("campos_publicos");
                Document match = new Document();

                match.append("coleccion", coleccion);   

                spic = collection.find(match, SPIC.class).first();

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener privacidad de campos: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return spic;
    }

    /**
     * Obtener el detalle del servidor p&uacute;blico seleccionado
     *
     * @param idServidorPublico Identificador del servidor p&uacute;blico
     *
     * @return Ficha de datos del servidor p&uacute;blico
     *
     * @throws Exception
     */
    public static SPIC obtenerDetalleServidorePublicoIntervienenContrataciones(String idServidorPublico) throws Exception
    {
        SPIC spic;                                                                              
        MongoClient conectarBaseDatos = null;                                                   
        MongoDatabase database;                                                                 
        MongoCollection<Document> collection;                                                   
        ArrayList<Document> query;                                                              
        Collation collation = Collation.builder().locale("es").caseLevel(true).build();
        ConfVariables confVariables;

        confVariables = new ConfVariables();  

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            spic = new SPIC();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("spic");

                query = new ArrayList<>();

                query.add(new Document("$match", new Document("_id", new ObjectId(idServidorPublico))));

                spic = collection.aggregate(query, SPIC.class).collation(collation).first();

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener datos del servidor p&uacute;blico: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return spic;
    }

    /**
     * Eliminar el servidor p&uacute;blico del listado del sistema 2,
     * seg&uacute;n sea elegido por el usuario
     *
     * @param id Identificador del servidor p&uacute;blico
     * @param usuario Usuario que realiza la modificaci&oacute;n
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     * @param servidorPublico
     *
     * @return Boolean
     *
     * @throws Exception
     */
    public static boolean eliminarServidorPublicoQueIntervieneContrataciones(String id, USUARIO usuario, NIVEL_ACCESO nivelAcceso, SPIC servidorPublico) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<SPIC> collection;                                       
        Document query;                                                         
        boolean seBorra;                                                        
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document spic;     
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                                       
        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();

        query = new Document();
        seBorra = false;

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("spic", SPIC.class);

                if (nivelAcceso.getId() == 1)
                { 
                    query.append("_id", new ObjectId(id));
                    seBorra = true;
                } 
                else if (nivelAcceso.getId() == 2)
                { 
                    query.append("_id", new ObjectId(id));
                    query.append("metadatos.usuario_registro", usuario.getUsuario());
                    seBorra = true;
                } 
                if (seBorra)
                { 
                    collection.deleteOne(query);
                    spic = Document.parse(gson.toJson(servidorPublico));
                    
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.spic, "SAEMM", servidorPublico.getId(), spic.toJson(), BitacoraPde.borrado));
                    
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al eliminar servidor p&uacute;blico: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return seBorra;
    }

    /**
     * Modificar datos del servidor p&uacute;blico registrado
     *
     * @param id Identificador del servidor p&uacute;blico a modificar
     * @param servidorPublico Nuevos datos del servidor p&uacute;blico
     * @param usuario Usuario que realiza la modificaci&oacute;n
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return Boolean
     *
     * @throws Exception
     */
    public static boolean editarServidorPublicoQueIntervieneContrataciones(String id, SPIC servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        Document where;                                                         
        boolean seEdita;                                                        
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document spic;         
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                                   
        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();
        where = new Document();
        seEdita = false;
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("spic");

                if (nivelAcceso.getId() == 1)
                { 
                    where.append("_id", new ObjectId(id));
                    seEdita = true;
                } 
                else if (nivelAcceso.getId() == 2)
                { 
                    where.append("_id", new ObjectId(id));
                    where.append("metadatos.usuario_registro", usuario.getUsuario());
                    seEdita = true;
                } 

                if (seEdita)
                { 

                    spic = Document.parse(gson.toJson(servidorPublico));
                    collection.updateOne(where, new Document("$set", spic));
                    
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.spic, "SAEMM", servidorPublico.getId(), spic.toJson(), BitacoraPde.modificacion));
                    

                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al modificar servidor p&uacute;blico: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return seEdita;
    }

    /**
     * Mostrar en la versi&oacute;n p&uacute;blica el registro realizado
     *
     * @param id Id del registro a publicar
     * @param servidorPublico Datos del servidor p&uacute;blico a actualizar
     * @param usuario Usuario que publica
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return
     *
     * @throws Exception
     */
    public static boolean publicarServidorPublicoQueIntervieneContrataciones(String id, SPIC servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        Document where;                                                         
        boolean sePublica;                                                      
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document spic;     
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                                       
        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();
        where = new Document();
        sePublica = false;
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("spic");

                if (nivelAcceso.getId() == 1)
                { 
                    where.append("_id", new ObjectId(id));
                    sePublica = true;
                } 
                else if (nivelAcceso.getId() == 2)
                { 
                    where.append("_id", new ObjectId(id));
                    where.append("metadatos.usuario_registro", usuario.getUsuario());
                    sePublica = true;
                } 
                if (sePublica)
                { 
                    spic = Document.parse(gson.toJson(servidorPublico));
                    collection.updateOne(where, new Document("$set", spic));
                    
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.spic, "SAEMM", servidorPublico.getId(), spic.toJson(), BitacoraPde.publicacion));
                    
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al publicar servidor p&uacute;blico: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return sePublica;
    }

    /**
     * Obtener tipo de área de un ID proporcionado
     *
     * @param claves
     *
     * @return Descripci&oacute;n del tipo de área
     *
     * @throws Exception
     */
    public static ArrayList<TIPO_AREA> obtenerTipoDeArea(ArrayList<String> claves) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_AREA> collection;                                  
        ArrayList<Document> where;                                              
        ArrayList<TIPO_AREA> tiposArea;    
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                       
        tiposArea = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                if (claves.size() > 0)
                { 
                    database = conectarBaseDatos.getDatabase(confVariables.getBD());
                    collection = database.getCollection("tipo_area", TIPO_AREA.class);
                    where = new ArrayList<>();
                    for (String clave : claves)
                    { 
                        where.add(new Document().append("clave", clave));
                    } 

                    tiposArea = collection.find(new Document("$or", where)).sort(new Document("valor", 1)).into(new ArrayList<>());

                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener area por id:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return tiposArea;
    }

    /**
     * Obtener el nivel de responsabilidad por Id
     *
     * @param clave C&oacute;digo del nivel de responsabilidad
     *
     * @return Descripci&oacute;n del nivel de responsabilidad
     *
     * @throws Exception
     */
    public static String obtenerNivelResponsabilidad(String clave) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        FindIterable<Document> aIPuesto;                                        
        MongoCursor<Document> cursor = null;                                    
        Document where;                                                         
        String area = "";                                                       
        NIVEL_RESPONSABILIDAD nivel_responsabilidad;
        JsonWriterSettings settings;                                            
        Gson gson;
        ConfVariables confVariables;

        confVariables = new ConfVariables();  

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("nivel_responsabilidad");
                where = new Document();
                where.append("clave", clave);

                aIPuesto = collection.find(where);
                cursor = aIPuesto.iterator();
                gson = new Gson();
                settings = JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build();

                if (cursor.hasNext())
                { 
                    Document next = cursor.next();
                    nivel_responsabilidad = gson.fromJson(next.toJson(settings), NIVEL_RESPONSABILIDAD.class);
                    area = nivel_responsabilidad.getValor();
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener nivel responsabilidad por id:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return area;
    }

    /**
     * Despublicar registro del servidor p&uacute;blico
     *
     * @param id Identificador del servidor p&uacute;blico
     * @param servidorPublico Datos del servidor p&uacute;blico
     * @param usuario Usuario de despublicaci&oacute;n
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return Boolean
     *
     * @throws Exception
     */
    public static boolean despublicarServidorPublicoQueIntervieneContrataciones(String id, SPIC servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        Document where;                                                         
        boolean seDesPublica;                                                   
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document spic;     
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                                       

        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();
        where = new Document();
        seDesPublica = false;
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("spic");

                if (nivelAcceso.getId() == 1)
                { 
                    where.append("_id", new ObjectId(id));
                    seDesPublica = true;
                } 
                else if (nivelAcceso.getId() == 2)
                { 
                    where.append("_id", new ObjectId(id));
                    where.append("metadatos.usuario_registro", usuario.getUsuario());
                    seDesPublica = true;
                } 
                if (seDesPublica)
                { 
                    spic = Document.parse(gson.toJson(servidorPublico));
                    collection.updateOne(where, new Document("$set", spic));
                    
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.spic, "SAEMM", servidorPublico.getId(), spic.toJson(), BitacoraPde.despublicacion));
                    
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al despublicar servidor p&uacute;blico: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return seDesPublica;
    }

    /**
     * Permite obtener el tipo de procedimientos
     *
     * @param tipo_procedimiento
     *
     * @return ArrayList que contiene los tipos de procedimientos de la base de
     * datos
     *
     * @throws Exception
     */
    public static ArrayList<TIPO_PROCEDIMIENTO> obtenerTipoProcedimientos(ArrayList<Integer> tipo_procedimiento) throws Exception
    {

        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_PROCEDIMIENTO> collection;                         
        ArrayList<TIPO_PROCEDIMIENTO> procedimientos;       
        ConfVariables confVariables;

        confVariables = new ConfVariables();                      
        procedimientos = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_procedimiento", TIPO_PROCEDIMIENTO.class);

                if (tipo_procedimiento.size() > 0)
                { 
                    ArrayList<Document> where = new ArrayList<>();
                    for (Integer procedimiento : tipo_procedimiento)
                    { 
                        where.add(new Document().append("clave", procedimiento));
                    } 

                    procedimientos = collection.find(new Document("$or", where)).sort(new Document("valor", 1)).into(new ArrayList<>());

                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener listado de tipos de procedimientos: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return procedimientos;
    }

    /**
     * Devuelve un genero en especifico usando como filtro de b&uacute;squeda el
     * c&oacute;digo del genero
     *
     * @param clave c&oacute;digo del genero que se buscara
     *
     * @return GENERO
     *
     * @throws java.lang.Exception
     */
    public static GENERO obtenerGenero(String clave) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<GENERO> collection;                                     
        GENERO genero;         
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                                   
        genero = new GENERO();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("genero", GENERO.class);

                genero = collection.find(new Document("clave", new Document("$eq", clave))).first();

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el genero de la base de datos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return genero;
    }

    public static ArrayList<NIVEL_RESPONSABILIDAD> obtenerNivelesDeResponsabilidad(ArrayList<String> claves) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<NIVEL_RESPONSABILIDAD> collection;                      
        ArrayList<NIVEL_RESPONSABILIDAD> listadoNivelResponsabilidad;           
        ArrayList<Document> where;     
        ConfVariables confVariables;

        confVariables = new ConfVariables();                                           
        where = new ArrayList<>();
        listadoNivelResponsabilidad = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("nivel_responsabilidad", NIVEL_RESPONSABILIDAD.class);
                if (claves.size() > 0)
                { 
                    for (String calve : claves)
                    { 
                        where.add(new Document().append("clave", calve));
                    } 

                    listadoNivelResponsabilidad = collection.find(new Document("$or", where)).sort(new Document("valor", 1)).into(new ArrayList<>());

                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener niveles de responsabilidad para el usuario:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoNivelResponsabilidad;

    }

    /**
     * Metodo de ayuda para cerrar las conexiones activas
     *
     *
     * @param conectarBaseDatos
     * @param cursor
     */
    public static void cerrarRecursos(MongoClient conectarBaseDatos, MongoCursor<Document> cursor)
    { 

        if (cursor != null)
        { 
            cursor.close();
            cursor = null;
        } 

        if (conectarBaseDatos != null)
        { 
            conectarBaseDatos.close();
            conectarBaseDatos = null;
        } 

    } 

}
