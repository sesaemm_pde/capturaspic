/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans.s2contrataciones;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */

public class FILTRO_BUSQUEDA_SPIC
  {
    private String ID;
    private String NOMBRES;
    private String PRIMER_APELLIDO;
    private String SEGUNDO_APELLIDO;
    private String CURP;
    private String INSTITUCION_DEPENDENCIA;
    private int registrosMostrar;
    private int numeroPagina;
    private String orden;
    
    public String getID()
      {
        return ID;
      }

    public void setID(String ID)
      {
        this.ID = ID;
      }

    public String getNOMBRES()
      {
        return NOMBRES;
      }

    public void setNOMBRES(String NOMBRES)
      {
        this.NOMBRES = NOMBRES;
      }

    public String getPRIMER_APELLIDO()
      {
        return PRIMER_APELLIDO;
      }

    public void setPRIMER_APELLIDO(String PRIMER_APELLIDO)
      {
        this.PRIMER_APELLIDO = PRIMER_APELLIDO;
      }

    public String getSEGUNDO_APELLIDO()
      {
        return SEGUNDO_APELLIDO;
      }

    public void setSEGUNDO_APELLIDO(String SEGUNDO_APELLIDO)
      {
        this.SEGUNDO_APELLIDO = SEGUNDO_APELLIDO;
      }

    public String getCURP()
      {
        return CURP;
      }

    public void setCURP(String CURP)
      {
        this.CURP = CURP;
      }

    public String getINSTITUCION_DEPENDENCIA()
      {
        return INSTITUCION_DEPENDENCIA;
      }

    public void setINSTITUCION_DEPENDENCIA(String INSTITUCION_DEPENDENCIA)
      {
        this.INSTITUCION_DEPENDENCIA = INSTITUCION_DEPENDENCIA;
      }

    public int getRegistrosMostrar()
      {
        return registrosMostrar;
      }

    public void setRegistrosMostrar(int registrosMostrar)
      {
        this.registrosMostrar = registrosMostrar;
      }

    public int getNumeroPagina()
      {
        return numeroPagina;
      }

    public void setNumeroPagina(int numeroPagina)
      {
        this.numeroPagina = numeroPagina;
      }

    public String getOrden()
      {
        return orden;
      }

    public void setOrden(String orden)
      {
        this.orden = orden;
      }
      
  }
