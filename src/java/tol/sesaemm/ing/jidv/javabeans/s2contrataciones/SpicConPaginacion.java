/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans.s2contrataciones;

import java.util.ArrayList;
import tol.sesaemm.ing.jidv.javabeans.PAGINACION;
import tol.sesaemm.javabeans.s2spic.SPIC;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
 
public class SpicConPaginacion
  {
    
    PAGINACION paginacion;
    ArrayList<SPIC> servidores;

    public SpicConPaginacion()
      {
      }

    public SpicConPaginacion(PAGINACION paginacion, ArrayList<SPIC> servidores)
      {
        this.paginacion = paginacion;
        this.servidores = servidores;
      }

    public PAGINACION getPaginacion()
      {
        return paginacion;
      }

    public void setPaginacion(PAGINACION paginacion)
      {
        this.paginacion = paginacion;
      }

    public ArrayList<SPIC> getServidores()
      {
        return servidores;
      }

    public void setServidores(ArrayList<SPIC> servidores)
      {
        this.servidores = servidores;
      }
    
  }
