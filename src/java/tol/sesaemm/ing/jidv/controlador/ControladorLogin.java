/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.controlador;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.javabeans.Miga;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.logicaDeNegocio.LogicaDeNegocioV01;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
 
public class ControladorLogin extends tol.sesaemm.ing.jidv.controlador.ControladorBase
{

    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException
    {
        String strAccion = "";
        LogicaDeNegocio LN = new LogicaDeNegocioV01();

        HttpSession sesion = request.getSession();
        USUARIO usuario;

        usuario = (USUARIO) sesion.getAttribute("usuario");
        strAccion = request.getParameter("accion");

        try
        {
            if (strAccion == null)
            {
                if (sesion != null)
                {
                    sesion.invalidate();
                    sesion = null;
                }

                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
            else if (strAccion.equals("paginaPrincipal"))
            {
                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
            else if (strAccion.equals("loguearUsuario"))
            {

                usuario = new USUARIO();
                usuario.setUsuario(request.getParameter("txtUsuario"));
                usuario.setPassword(request.getParameter("txtPassword"));

                usuario = LN.loguearUsuario(usuario);

                if (usuario != null)
                {

                    sesion = request.getSession(true);
                    sesion.setAttribute("usuario", usuario);

                    LN.registrarAcceso(usuario.getUsuario());
                    request.setAttribute("sistemas", LN.obtenerDatosActualizadosDeSistemas(usuario.getSistemas()));

                    this.fordward(request, resp, "/frmMenuSistemas.jsp");

                }
                else
                {
                    if (sesion != null)
                    {
                        sesion.invalidate();
                        sesion = null;
                    }

                    request.setAttribute("mensaje", "Usuario o contraseña incorrectos");
                    this.fordward(request, resp, "/frmPrincipal.jsp");
                }

            }
            else if (strAccion.equals("cerrarSesion"))
            {
                if (sesion != null)
                {
                    sesion.invalidate();
                    sesion = null;
                }

                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
            else if (usuario != null)
            {
                if (strAccion.equals("mostrarMenuSistemas"))
                {

                    request.setAttribute("sistemas", LN.obtenerDatosActualizadosDeSistemas(usuario.getSistemas()));
                    this.fordward(request, resp, "/frmMenuSistemas.jsp");
                }
                else if (strAccion.equals("mostrarPaginaQueEs"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("¿Qué es la PDE?", ""));

                    request.setAttribute("migas", migas);
                    this.fordward(request, resp, "/queEs.jsp");
                }
                else if (strAccion.equals("mostrarPaginaPreguntasFrecuentes"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Preguntas frecuentes", ""));

                    request.setAttribute("migas", migas);
                    this.fordward(request, resp, "/faq.jsp");
                }
                else if (strAccion.equals("mostrarPaginaTerminos"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Términos de uso", ""));

                    request.setAttribute("migas", migas);
                    this.fordward(request, resp, "/terminos.jsp");
                }
                else if (strAccion.equals("mostrarPaginaPrivacidad"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Aviso de privacidad", ""));

                    request.setAttribute("migas", migas);
                    this.fordward(request, resp, "/privacidad.jsp");
                }
            }
            else
            {
                if (sesion != null)
                {
                    sesion.invalidate();
                    sesion = null;
                }

                request.setAttribute("mensaje", "Su sesi&oacute;n ha expirado ");
                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
        }
        catch (ServletException | IOException ex)
        {
            request.setAttribute("mensaje", "Error : " + ex.toString());
            this.fordward(request, resp, "/error.jsp");
        }
        catch (Exception ex)
        {
            request.setAttribute("mensaje", "Error " + ex.toString());
            this.fordward(request, resp, "/error.jsp");
        }
    }

    public void fordward(HttpServletRequest req, HttpServletResponse resp, String ruta) throws ServletException, IOException
    {
        req.setAttribute("datosDeLaUltimaActualizacion", ConfVariables.getDATOS_ACTUALIZACION());
        RequestDispatcher despachador = req.getRequestDispatcher(ruta);
        despachador.forward(req, resp);
    }
}
