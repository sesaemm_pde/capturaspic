/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.controlador;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocioS2;
import tol.sesaemm.ing.jidv.javabeans.Miga;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.PAGINACION;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s2contrataciones.FILTRO_BUSQUEDA_SPIC;
import tol.sesaemm.ing.jidv.javabeans.s2contrataciones.SpicConPaginacion;
import tol.sesaemm.ing.jidv.logicaDeNegocio.LogicaDeNegocioS2V01;
import tol.sesaemm.ing.jidv.logicaDeNegocio.LogicaDeNegocioV01;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.GENERO;
import tol.sesaemm.javabeans.PUESTO;
import tol.sesaemm.javabeans.RAMO;
import tol.sesaemm.javabeans.s2spic.NIVEL_RESPONSABILIDAD;
import tol.sesaemm.javabeans.s2spic.SPIC;
import tol.sesaemm.javabeans.s2spic.TIPO_AREA;
import tol.sesaemm.javabeans.s2spic.TIPO_PROCEDIMIENTO;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
 
public class ControladorContrataciones extends tol.sesaemm.ing.jidv.controlador.ControladorBase
{

    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException
    {
        String strAccion = "";
        LogicaDeNegocio LN = new LogicaDeNegocioV01();
        LogicaDeNegocioS2 LNS2 = new LogicaDeNegocioS2V01();
        HttpSession sesion = request.getSession();
        USUARIO usuario;
        NIVEL_ACCESO nivelAcceso;

        usuario = (USUARIO) sesion.getAttribute("usuario");
        strAccion = request.getParameter("accion");

        try
        {
            if (strAccion == null)
            {
                if (sesion != null)
                {
                    sesion.invalidate();
                    sesion = null;
                }

                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
            else if (usuario != null)
            {
                nivelAcceso = LN.tipoDeUsuario(usuario, 2);

                if (strAccion.equals("ingresarSistema"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    PAGINACION paginacion;

                    paginacion = new PAGINACION();
                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos que intervienen en contrataciones", "", "contrataciones"));

                    request.setAttribute("migas", migas);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", LN.tipoDeUsuario(usuario, 2));
                    this.fordward(request, resp, "s2ContratacionesPublicas/frmPaginaPrincipalContrataciones.jsp");

                }
                else if (strAccion.equals("consultarServidoresPublicosEnContrataciones"))
                {
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<SPIC> spic;
                    ArrayList<Miga> migas = new ArrayList<>();
                    FILTRO_BUSQUEDA_SPIC filtroBusqueda;
                    PAGINACION paginacion;
                    SpicConPaginacion spicConPaginacion;

                    paginacion = new PAGINACION();
                    spic = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos que intervienen en contrataciones", "", "contrataciones"));

                    filtroBusqueda = LNS2.modelarFiltroSpic(request);
                    dependenciasDelUsuario = LNS2.obtenerDependenciasDelUsuario(usuario);
                    spicConPaginacion = LNS2.obtenerListadoDeServidoresPublicosQueIntervienenContrataciones(usuario, dependenciasDelUsuario, filtroBusqueda);

                    if (spicConPaginacion != null)
                    {
                        paginacion = spicConPaginacion.getPaginacion();
                        spic = spicConPaginacion.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("servidoresPublicos", spic);
                    this.fordward(request, resp, "s2ContratacionesPublicas/frmPaginaPrincipalContrataciones.jsp");
                }
                else if (strAccion.equals("mostrarPaginaRegistroNuevaContrataciones"))
                {
                    ArrayList<NIVEL_RESPONSABILIDAD> nivelesDeResponsabilidad;
                    ArrayList<TIPO_PROCEDIMIENTO> tipoProcedimientos;
                    ArrayList<Miga> migas = new ArrayList<>();
                    ArrayList<ENTE_PUBLICO> entesPublicos;
                    ArrayList<TIPO_AREA> tipoDeArea;
                    ArrayList<PUESTO> puestos;
                    ArrayList<RAMO> ramos;
                    ArrayList<GENERO> generos;

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos que intervienen en contrataciones", "ingresarSistema", "contrataciones"));
                    migas.add(new Miga("Registro de servidor p&uacute;blico", "", "contrataciones"));

                    entesPublicos = LNS2.obtenerEntesPublicos(usuario.getDependencias(), usuario);
                    nivelesDeResponsabilidad = LNS2.obtenerNivelesDeResponsabilidad();
                    tipoProcedimientos = LNS2.obtenerTipoProcedimientos();
                    tipoDeArea = LNS2.obtenerTiposDeArea();
                    puestos = LNS2.obtenerPuestos();
                    ramos = LNS2.obtenerRamos();
                    generos = LNS2.obtenerGeneros();

                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("puestos", puestos);
                    request.setAttribute("tipoDeArea", tipoDeArea);
                    request.setAttribute("nivelesDeResponsabilidad", nivelesDeResponsabilidad);
                    request.setAttribute("tipoProcedimientos", tipoProcedimientos);
                    request.setAttribute("migas", migas);
                    request.setAttribute("ramos", ramos);
                    request.setAttribute("generos", generos);
                    this.fordward(request, resp, "s2ContratacionesPublicas/frmRegistraServidorPublicoContrataciones.jsp");

                }
                else if (strAccion.equals("mostrarVistaPreviaServidorPublicoContrataciones"))
                {
                    SPIC servidorPublico;
                    ArrayList<Miga> migas = new ArrayList<>();
                    ArrayList<TIPO_PROCEDIMIENTO> tipo_procedimiento;
                    ArrayList<NIVEL_RESPONSABILIDAD> nivelesDeResponsabilidad;

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos que intervienen en contrataciones", "ingresarSistema", "contrataciones"));
                    migas.add(new Miga("Registro de servidor p&uacute;blico", "", "contrataciones"));

                    servidorPublico = LNS2.modelarRequest(request);

                    request.setAttribute("migas", migas);
                    request.setAttribute("servidorPublico", servidorPublico);
                    this.fordward(request, resp, "s2ContratacionesPublicas/frmVistaPreviaServidorPublicoContrataciones.jsp");

                }
                else if (strAccion.equals("registrarServidorPublicoContrataciones"))
                {
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<SPIC> spic;
                    ArrayList<Miga> migas = new ArrayList<>();
                    FILTRO_BUSQUEDA_SPIC filtroBusqueda;
                    PAGINACION paginacion;
                    SpicConPaginacion spicConPaginacion;
                    SPIC servidorPublico;
                    String mensaje;

                    paginacion = new PAGINACION();
                    spic = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos que intervienen en contrataciones", "ingresarSistema", "contrataciones"));
                    migas.add(new Miga("Registro de servidor p&uacute;blico", "", "contrataciones"));

                    servidorPublico = LNS2.modelarRequest(request);
                    mensaje = LNS2.registrarServidorPublicoQueIntervieneContrataciones(servidorPublico, usuario, nivelAcceso);

                    filtroBusqueda = LNS2.modelarFiltroSpic(request);
                    dependenciasDelUsuario = LNS2.obtenerDependenciasDelUsuario(usuario);
                    spicConPaginacion = LNS2.obtenerListadoDeServidoresPublicosQueIntervienenContrataciones(usuario, dependenciasDelUsuario, filtroBusqueda);

                    if (spicConPaginacion != null)
                    {
                        paginacion = spicConPaginacion.getPaginacion();
                        spic = spicConPaginacion.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("correcto", true);
                    request.setAttribute("mensaje", mensaje);
                    request.setAttribute("servidoresPublicos", spic);
                    this.fordward(request, resp, "s2ContratacionesPublicas/frmPaginaPrincipalContrataciones.jsp");
                }
                else if (strAccion.equals("modificarDatosServidorPublicoContrataciones"))
                {

                    ArrayList<NIVEL_RESPONSABILIDAD> nivelesDeResponsabilidad;
                    ArrayList<TIPO_PROCEDIMIENTO> tipoProcedimientos;
                    ArrayList<ENTE_PUBLICO> entesPublicos;
                    ArrayList<TIPO_AREA> tipoDeArea;
                    ArrayList<PUESTO> puestos;
                    ArrayList<RAMO> ramos;
                    SPIC servidorPublico;
                    ArrayList<GENERO> generos;

                    ArrayList<Miga> migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos que intervienen en contrataciones", "ingresarSistema", "contrataciones"));
                    migas.add(new Miga("Registro de servidor p&uacute;blico", "", "contrataciones"));

                    servidorPublico = LNS2.modelarRequest(request);
                    entesPublicos = LNS2.obtenerEntesPublicos(usuario.getDependencias(), usuario);
                    nivelesDeResponsabilidad = LNS2.obtenerNivelesDeResponsabilidad();
                    tipoProcedimientos = LNS2.obtenerTipoProcedimientos();
                    tipoDeArea = LNS2.obtenerTiposDeArea();
                    puestos = LNS2.obtenerPuestos();
                    generos = LNS2.obtenerGeneros();
                    ramos = LNS2.obtenerRamos();

                    request.setAttribute("nivelesDeResponsabilidad", nivelesDeResponsabilidad);
                    request.setAttribute("tipoProcedimientos", tipoProcedimientos);
                    request.setAttribute("servidorPublico", servidorPublico);
                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("tipoDeArea", tipoDeArea);
                    request.setAttribute("puestos", puestos);
                    request.setAttribute("generos", generos);
                    request.setAttribute("ramos", ramos);
                    request.setAttribute("migas", migas);
                    this.fordward(request, resp, "s2ContratacionesPublicas/frmRegistraServidorPublicoContrataciones.jsp");

                }
                else if (strAccion.equals("detalleContrataciones"))
                {

                    SPIC estatusDePrivacidadCamposSPIC;
                    SPIC servidoresPublicosIntervienenContrataciones;
                    ArrayList<Miga> migas;
                    FILTRO_BUSQUEDA_SPIC filtro;

                    filtro = LNS2.modelarFiltroSpic(request);

                    servidoresPublicosIntervienenContrataciones = LNS2.obtenerDetalleServidorPublicoIntervieneContrataciones(filtro.getID());
                    estatusDePrivacidadCamposSPIC = LNS2.obtenerEstatusDePrivacidadCamposSPIC();

                    migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos que intervienen en contrataciones", "ingresarSistema", "contrataciones"));
                    migas.add(new Miga("Registro de servidor p&uacute;blico", "", "contrataciones"));

                    request.setAttribute("migas", migas);
                    request.setAttribute("privacidadDatos", estatusDePrivacidadCamposSPIC);
                    request.setAttribute("contrataciones", servidoresPublicosIntervienenContrataciones);
                    request.setAttribute("mujer", LNS2.esMujer(servidoresPublicosIntervienenContrataciones.getCurp()));
                    request.setAttribute("filtro", filtro);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    this.fordward(request, resp, "s2ContratacionesPublicas/frmDetalleServidorPublicoContrataciones.jsp");
                }
                else if (strAccion.equals("eliminarContrataciones"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    FILTRO_BUSQUEDA_SPIC filtro;
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<SPIC> spic;
                    String mensaje;
                    PAGINACION paginacion;
                    SpicConPaginacion spicConPaginacion;
                    SPIC servidorPublico;

                    paginacion = new PAGINACION();
                    spic = new ArrayList<>();

                    filtro = LNS2.modelarFiltroSpic(request);
                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos que intervienen en contrataciones", "", "contrataciones"));

                    servidorPublico = LNS2.obtenerDetalleServidorPublicoIntervieneContrataciones(filtro.getID());

                    mensaje = LNS2.eliminarServidorPublicoQueIntervieneContrataciones(filtro.getID(), usuario, nivelAcceso, servidorPublico);

                    dependenciasDelUsuario = LNS2.obtenerDependenciasDelUsuario(usuario);
                    spicConPaginacion = LNS2.obtenerListadoDeServidoresPublicosQueIntervienenContrataciones(usuario, dependenciasDelUsuario, filtro);

                    if (spicConPaginacion != null)
                    {
                        paginacion = spicConPaginacion.getPaginacion();
                        spic = spicConPaginacion.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtro);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("servidoresPublicos", spic);
                    request.setAttribute("correcto", true);
                    request.setAttribute("mensaje", mensaje);
                    this.fordward(request, resp, "s2ContratacionesPublicas/frmPaginaPrincipalContrataciones.jsp");
                }
                else if (strAccion.equals("mostrarPaginaEditarContrataciones"))
                {
                    ArrayList<NIVEL_RESPONSABILIDAD> nivelesDeResponsabilidad;
                    ArrayList<TIPO_PROCEDIMIENTO> tipoProcedimientos;
                    ArrayList<ENTE_PUBLICO> entesPublicos;
                    ArrayList<TIPO_AREA> tipoDeArea;
                    ArrayList<PUESTO> puestos;
                    ArrayList<RAMO> ramos;
                    ArrayList<GENERO> generos;
                    SPIC servidorPublico;
                    FILTRO_BUSQUEDA_SPIC filtro;

                    filtro = LNS2.modelarFiltroSpic(request);

                    ArrayList<Miga> migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos que intervienen en contrataciones", "ingresarSistema", "contrataciones"));
                    migas.add(new Miga("Edici&oacute;n de servidor p&uacute;blico", "", "contrataciones"));

                    servidorPublico = LNS2.obtenerDetalleServidorPublicoIntervieneContrataciones(filtro.getID());
                    servidorPublico.setId(filtro.getID());
                    entesPublicos = LNS2.obtenerEntesPublicos(usuario.getDependencias(), usuario);
                    nivelesDeResponsabilidad = LNS2.obtenerNivelesDeResponsabilidad();
                    tipoProcedimientos = LNS2.obtenerTipoProcedimientos();
                    tipoDeArea = LNS2.obtenerTiposDeArea();
                    puestos = LNS2.obtenerPuestos();
                    ramos = LNS2.obtenerRamos();
                    generos = LNS2.obtenerGeneros();

                    request.setAttribute("migas", migas);
                    request.setAttribute("servidorPublico", servidorPublico);
                    request.setAttribute("filtro", filtro);
                    request.setAttribute("tipoDeArea", tipoDeArea);
                    request.setAttribute("ramos", ramos);
                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("puestos", puestos);
                    request.setAttribute("generos", generos);
                    request.setAttribute("tipoProcedimientos", tipoProcedimientos);
                    request.setAttribute("nivelesDeResponsabilidad", nivelesDeResponsabilidad);
                    this.fordward(request, resp, "s2ContratacionesPublicas/frmEditaServidorPublicoContrataciones.jsp");
                }
                else if (strAccion.equals("editarServidorPublicoContrataciones"))
                {
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<SPIC> spic;
                    ArrayList<Miga> migas = new ArrayList<>();
                    SPIC servidorPublico;
                    FILTRO_BUSQUEDA_SPIC filtro;
                    String mensaje;
                    PAGINACION paginacion;
                    SpicConPaginacion spicConPaginacion;

                    paginacion = new PAGINACION();
                    spic = new ArrayList<>();

                    filtro = LNS2.modelarFiltroSpic(request);

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos que intervienen en contrataciones", "", "contrataciones"));

                    servidorPublico = LNS2.obtenerDetalleServidorPublicoIntervieneContrataciones(filtro.getID());
                    servidorPublico = LNS2.modelarRequest(request, servidorPublico);
                    mensaje = LNS2.editarServidorPublicoQueIntervieneContrataciones(filtro.getID(), servidorPublico, usuario, nivelAcceso);

                    dependenciasDelUsuario = LNS2.obtenerDependenciasDelUsuario(usuario);
                    spicConPaginacion = LNS2.obtenerListadoDeServidoresPublicosQueIntervienenContrataciones(usuario, dependenciasDelUsuario, filtro);
                    if (spicConPaginacion != null)
                    {
                        paginacion = spicConPaginacion.getPaginacion();
                        spic = spicConPaginacion.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtro);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("correcto", true);
                    request.setAttribute("servidoresPublicos", spic);
                    request.setAttribute("mensaje", mensaje);
                    this.fordward(request, resp, "s2ContratacionesPublicas/frmPaginaPrincipalContrataciones.jsp");
                }
                else if (strAccion.equals("publicarContrataciones"))
                {
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<SPIC> spic;
                    ArrayList<Miga> migas = new ArrayList<>();
                    String id;
                    SPIC servidorPublicoIntervieneContrataciones;
                    FILTRO_BUSQUEDA_SPIC filtro;
                    String mensaje;
                    PAGINACION paginacion;
                    SpicConPaginacion spicConPaginacion;

                    paginacion = new PAGINACION();
                    spic = new ArrayList<>();

                    filtro = LNS2.modelarFiltroSpic(request);
                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos que intervienen en contrataciones", "", "contrataciones"));

                    id = request.getParameter("txtId");

                    servidorPublicoIntervieneContrataciones = LNS2.obtenerDetalleServidorPublicoIntervieneContrataciones(id);
                    mensaje = LNS2.publicarServidorPublicoQueIntervieneContrataciones(id, servidorPublicoIntervieneContrataciones, usuario, nivelAcceso);

                    dependenciasDelUsuario = LNS2.obtenerDependenciasDelUsuario(usuario);
                    spicConPaginacion = LNS2.obtenerListadoDeServidoresPublicosQueIntervienenContrataciones(usuario, dependenciasDelUsuario, filtro);
                    if (spicConPaginacion != null)
                    {
                        paginacion = spicConPaginacion.getPaginacion();
                        spic = spicConPaginacion.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtro);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("servidoresPublicos", spic);
                    request.setAttribute("correcto", true);
                    request.setAttribute("mensaje", mensaje);
                    this.fordward(request, resp, "s2ContratacionesPublicas/frmPaginaPrincipalContrataciones.jsp");
                }
                else if (strAccion.equals("despublicarContrataciones"))
                {
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<SPIC> spic;
                    ArrayList<Miga> migas = new ArrayList<>();
                    SPIC servidorPublicoIntervieneContrataciones;
                    FILTRO_BUSQUEDA_SPIC filtro;
                    String mensaje;
                    PAGINACION paginacion;
                    SpicConPaginacion spicConPaginacion;

                    paginacion = new PAGINACION();
                    spic = new ArrayList<>();

                    filtro = LNS2.modelarFiltroSpic(request);

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos que intervienen en contrataciones", "", "contrataciones"));

                    servidorPublicoIntervieneContrataciones = LNS2.obtenerDetalleServidorPublicoIntervieneContrataciones(filtro.getID());
                    mensaje = LNS2.despublicarServidorPublicoQueIntervieneContrataciones(filtro.getID(), servidorPublicoIntervieneContrataciones, usuario, nivelAcceso);

                    dependenciasDelUsuario = LNS2.obtenerDependenciasDelUsuario(usuario);
                    spicConPaginacion = LNS2.obtenerListadoDeServidoresPublicosQueIntervienenContrataciones(usuario, dependenciasDelUsuario, filtro);
                    if (spicConPaginacion != null)
                    {
                        paginacion = spicConPaginacion.getPaginacion();
                        spic = spicConPaginacion.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtro);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("correcto", true);
                    request.setAttribute("servidoresPublicos", spic);
                    request.setAttribute("mensaje", mensaje);
                    this.fordward(request, resp, "s2ContratacionesPublicas/frmPaginaPrincipalContrataciones.jsp");
                }
                else if (strAccion.equals("mostrarPaginaQueEs"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital", "paginaPrincipal"));
                    migas.add(new Miga("¿Qu&eacute; es la PDE?", ""));

                    request.setAttribute("migas", migas);
                    this.fordward(request, resp, "/queEs.jsp");
                }
                else if (strAccion.equals("mostrarPaginaPreguntasFrecuentes"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital", "paginaPrincipal"));
                    migas.add(new Miga("Preguntas frecuentes", ""));

                    request.setAttribute("migas", migas);
                    this.fordward(request, resp, "/faq.jsp");
                }
                else if (strAccion.equals("mostrarPaginaTerminos"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital", "paginaPrincipal"));
                    migas.add(new Miga("T&eacute;rminos de uso", ""));

                    request.setAttribute("migas", migas);
                    this.fordward(request, resp, "/terminos.jsp");
                }
                else if (strAccion.equals("mostrarPaginaPrivacidad"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital", "paginaPrincipal"));
                    migas.add(new Miga("Aviso de privacidad", ""));

                    request.setAttribute("migas", migas);
                    this.fordward(request, resp, "/privacidad.jsp");
                }
                else
                {
                    if (sesion != null)
                    {
                        sesion.invalidate();
                        sesion = null;
                    }

                    request.setAttribute("mensaje", "Acci&oacute;n incorrecta: " + strAccion);
                    this.fordward(request, resp, "error.jsp");
                }
            }
            else
            {
                if (sesion != null)
                {
                    sesion.invalidate();
                    sesion = null;
                }

                request.setAttribute("mensaje", "Su sesi&oacute;n ha expirado ");
                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
        }
        catch (ServletException | IOException ex)
        {
            request.setAttribute("mensaje", "Error : " + ex.toString());
            this.fordward(request, resp, "/error.jsp");
        }
        catch (Exception ex)
        {
            request.setAttribute("mensaje", "Error " + ex.toString());
            this.fordward(request, resp, "/error.jsp");
        }
    }

    public void fordward(HttpServletRequest req, HttpServletResponse resp, String ruta) throws ServletException, IOException
    {
        req.setAttribute("datosDeLaUltimaActualizacion", ConfVariables.getDATOS_ACTUALIZACION());
        RequestDispatcher despachador = req.getRequestDispatcher(ruta);
        despachador.forward(req, resp);
    }
}
